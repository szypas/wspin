package szymek.wspin;

import lombok.SneakyThrows;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.JwtRequest;
import szymek.wspin.dto.JwtResponse;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;

import java.net.URI;
import java.util.List;

@Service
public class WspinClient {
    private static final String ROUTES_URL = "http://localhost:8080/routes";
    private static final String USERS_URL = "http://localhost:8080/users";
    private static final String AUTHENTICATE_URL = "http://localhost:8080/authenticateRest";
    private static final String ASCENTS_URL = "http://localhost:8080/ascents";
    private final RestTemplate restTemplate = new RestTemplate();

    public ResponseEntity<JwtResponse> login(String userName, String password) {
        JwtRequest jwtRequest = new JwtRequest(userName, password);
        return restTemplate.postForEntity(AUTHENTICATE_URL, jwtRequest, JwtResponse.class);
    }

    public ResponseEntity<List> getAllRoutes() {
        return restTemplate.getForEntity(ROUTES_URL, List.class);
    }

    public ResponseEntity<List> getAllUsers(String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, token);
        return restTemplate.exchange(USERS_URL, HttpMethod.GET, request, List.class);
    }

    public ResponseEntity<UserDto> registerAdmin(UserDto userDto, String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(userDto, token);
        return restTemplate.exchange(USERS_URL + "-registerAdmin", HttpMethod.POST, request, UserDto.class);
    }

    public ResponseEntity<UserDto> registerUser(UserDto userDto) {
        return restTemplate.postForEntity(USERS_URL, userDto, UserDto.class);
    }

    public void deleteUser(String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, token);
        restTemplate.exchange(USERS_URL, HttpMethod.DELETE, request, String.class);
    }

    private HttpEntity<?> createHttpEntityWithAuthBearer(Object userDto, String token) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + token);
        return new HttpEntity<>(userDto, headers);
    }

    public ResponseEntity<RouteDto> createRoute(RouteDto routeDto, String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(routeDto, token);
        return restTemplate.exchange(ROUTES_URL, HttpMethod.POST, request, RouteDto.class);
    }

    public void deleteRoute(String routeId, String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, token);
        restTemplate.exchange(ROUTES_URL + "/" + routeId, HttpMethod.DELETE, request, String.class);
    }

    public ResponseEntity<List> getAscents(String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, token);
        return restTemplate.exchange(ASCENTS_URL, HttpMethod.GET, request, List.class);
    }

    public ResponseEntity<AscentDto> createAscent(AscentDto ascentDto, String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(ascentDto, token);
        return restTemplate.exchange(ASCENTS_URL, HttpMethod.POST, request, AscentDto.class);
    }

    public void deleteAscent(String ascentId, String token) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, token);
        restTemplate.exchange(ASCENTS_URL + "/" + ascentId, HttpMethod.DELETE, request, String.class);
    }

    @SneakyThrows
    public ResponseEntity<List> getFilteredAscents(String userToken1, String params) {
        HttpEntity<?> request = createHttpEntityWithAuthBearer(null, userToken1);
        return restTemplate.exchange(new URI(ASCENTS_URL + "?" + params), HttpMethod.GET, request, List.class);
    }
}
