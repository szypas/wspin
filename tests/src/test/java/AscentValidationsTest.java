import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;
import szymek.wspin.WspinClient;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;
import szymek.wspin.util.Status;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class AscentValidationsTest {

    private static final String USER_NAME_1 = "newUser";
    private static final String USER_EMAIL_1 = "user@email";
    private static final String USER_PASS_1 = "123u";
    private static String USER_TOKEN_1;
    private static final WspinClient wspinClient = new WspinClient();
    RouteDto routeDto = new RouteDto("ROUTE_NAME", "WALL_NAME", "CRAG_NAME");

    @BeforeAll
    static void setup() {
        wspinClient.registerUser(new UserDto(USER_NAME_1, USER_EMAIL_1, USER_PASS_1));
        USER_TOKEN_1 = Objects.requireNonNull(wspinClient.login(USER_NAME_1, USER_PASS_1).getBody()).getToken();
    }

    @AfterAll
    static void cleanup() {
        wspinClient.deleteUser(USER_TOKEN_1);
    }

    @Test
    void whenNullRoute_thenBadRequest() {
        AscentDto ascentDto = new AscentDto(null, "V", Status.OS,
                Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        assertThrows(HttpClientErrorException.BadRequest.class, () -> wspinClient.createAscent(ascentDto, USER_TOKEN_1));
    }

    @Test
    void whenInvalidDifficulty_thenBadRequest() {
        AscentDto ascentDto = new AscentDto(routeDto, "invalid", Status.OS,
                Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        assertThrows(HttpClientErrorException.BadRequest.class, () -> wspinClient.createAscent(ascentDto, USER_TOKEN_1));
    }

    @Test
    void whenNullStatus_thenBadRequest() {
        AscentDto ascentDto = new AscentDto(routeDto, "V", null,
                Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        assertThrows(HttpClientErrorException.BadRequest.class, () -> wspinClient.createAscent(ascentDto, USER_TOKEN_1));
    }
}
