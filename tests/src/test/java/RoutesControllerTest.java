import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import szymek.wspin.WspinClient;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RoutesControllerTest {
    private static final String USER_NAME = "newUser";
    private static final String USER_EMAIL = "user@email";
    private static final String USER_PASS = "123u";
    private static final String ROUTE_NAME = "myRoute";
    private static final String WALL_NAME = "myWall";
    private static final String CRAG_NAME = "myCrag";
    private static String ROUTE_ID;
    private static String USER_TOKEN;
    private static final WspinClient wspinClient = new WspinClient();

    @BeforeAll
    static void setup() {
        wspinClient.registerUser(new UserDto(USER_NAME, USER_EMAIL, USER_PASS));
        USER_TOKEN = Objects.requireNonNull(wspinClient.login(USER_NAME, USER_PASS).getBody()).getToken();
    }

    @AfterAll
    static void cleanup() {
        wspinClient.deleteUser(USER_TOKEN);
    }

    @Test
    @Order(10)
    void anonymousCantPostRoutes() {
        RouteDto routeDto = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.createRoute(routeDto, null));
    }

    @Test
    @Order(20)
    void userCanPostRoutes() {
        RouteDto routeDto = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        ResponseEntity<RouteDto> response = wspinClient.createRoute(routeDto, USER_TOKEN);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        System.out.println(response.getBody());
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getCragName()).isEqualTo(CRAG_NAME);
        assertThat(response.getBody().getRouteName()).isEqualTo(ROUTE_NAME);
        assertThat(response.getBody().getWallName()).isEqualTo(WALL_NAME);
        ROUTE_ID = response.getBody().getId();
    }

    @Test
    @Order(30)
    void anonymousCanGetRoutes() {
        ResponseEntity<List> response = wspinClient.getAllRoutes();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).hasSize(1);
        assertThat(response.getBody().toString()).contains(ROUTE_NAME, WALL_NAME, CRAG_NAME);
    }

    @Test
    @Order(40)
    void anonymousCantDeleteRoutes() {
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.deleteRoute(ROUTE_ID, null));

    }

    @Test
    @Order(50)
    void userCanDeleteRoutes() {
        wspinClient.deleteRoute(ROUTE_ID, USER_TOKEN);
        ResponseEntity<List> response = wspinClient.getAllRoutes();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().toString()).doesNotContain(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        assertThat(response.getBody()).isEmpty();
    }
}
