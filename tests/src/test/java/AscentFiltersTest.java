import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import szymek.wspin.WspinClient;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;
import szymek.wspin.util.Status;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class AscentFiltersTest {

    private static final String USER_NAME_1 = "newUser";
    private static final String USER_EMAIL_1 = "user@email";
    private static final String USER_PASS_1 = "123u";
    private static final List<String> ROUTE_IDS = new ArrayList<>();
    private static final List<String> ASCENT_IDS = new ArrayList<>();
    private static final WspinClient wspinClient = new WspinClient();
    private static String USER_TOKEN_1;

    @BeforeAll
    static void setup() {
        wspinClient.registerUser(new UserDto(USER_NAME_1, USER_EMAIL_1, USER_PASS_1));
        USER_TOKEN_1 = Objects.requireNonNull(wspinClient.login(USER_NAME_1, USER_PASS_1).getBody()).getToken();
        addAscentWithRoute("obladi-oblada", "labajowa sciana", "grupa labajowej",
                "VI.2+", Status.FL, createUtilDate(2017, 7, 15));
        addAscentWithRoute("sekret mnicha", "mniszkowa", "grupa labajowej",
                "V", Status.FL, createUtilDate(2018, 7, 15));
        addAscentWithRoute("inny swiat", "cyganska turnia", "brzoskwinia",
                "VI.2+", Status.Todo, null);
        addAscentWithRoute("pojedynek z wiatrem", "przelaz", "sloneczne skaly",
                "VI.4", Status.Todo_Tried, null);
        addAscentWithRoute("spitostrada", "ostatnia", "brzoskwinia",
                "VI.2+", Status.OS, createUtilDate(2016, 7, 15));
        addAscentWithRoute("hipokryzja", "cyganska turnia", "brzoskwinia",
                "VI.4", Status.RP, createUtilDate(2015, 7, 15));
        addAscentWithRoute("hiszpanskie czary", "zytnie skaly", "zytnie skaly",
                "VI.3+", Status.RP, createUtilDate(2017, 7, 15));
        addAscentWithRoute("francuskie stosy", "zytnie skaly", "zytnie skaly",
                "VI.2", Status.OS, createUtilDate(2018, 7, 15));
        addAscentWithRoute("sadystowka", "sadystowka", "zakrzowek",
                "VI.3", Status.Project, null);
        addAscentWithRoute("prawy meningitis", "murarze", "dupa slonia",
                "VI.2+", Status.Todo_Tried, null);
        addAscentWithRoute("filar lesnego dziadka", "lyse plecy", "lyse skaly",
                "VI.1+", Status.OS, createUtilDate(2019, 7, 15));
        addAscentWithRoute("beatyfikacja", "kazalnica", "brzoskwinia",
                "VI.1+", Status.RP, createUtilDate(2020, 7, 15));
        System.out.println(USER_TOKEN_1);
    }

    @AfterAll
    static void cleanup() {
        ASCENT_IDS.forEach(id -> wspinClient.deleteAscent(id, USER_TOKEN_1));
        ROUTE_IDS.forEach(id -> wspinClient.deleteRoute(id, USER_TOKEN_1));
        wspinClient.deleteUser(USER_TOKEN_1);
    }

    @Test
    void filterDate() {
        String params = "dateFrom=2017-01-01&dateTo=2019-01-01";
        ResponseEntity<List> result = wspinClient.getFilteredAscents(USER_TOKEN_1, params);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotEmpty();
        assertThat(result.getBody()).hasSize(4);
    }

    @Test
    void filterStatuses() {
        String params = "statuses=Todo,FL,OS";
        ResponseEntity<List> result = wspinClient.getFilteredAscents(USER_TOKEN_1, params);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotEmpty();
        assertThat(result.getBody()).hasSize(6);
    }

    @Test
    void filterDifficulty() {
        String params = "difficultyFrom=VI.2&difficultyTo=VI.3%2B";
        ResponseEntity<List> result = wspinClient.getFilteredAscents(USER_TOKEN_1, params);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotEmpty();
        assertThat(result.getBody()).hasSize(7);
    }

    @Test
    void filterAll() {
        String params = "difficultyFrom=VI.2&difficultyTo=VI.3%2B&statuses=RP,OS&dateTo=2020-12-30&dateFrom=2017-01-01";
        ResponseEntity<List> result = wspinClient.getFilteredAscents(USER_TOKEN_1, params);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotEmpty();
        assertThat(result.getBody()).hasSize(2);
    }

    private static Date createUtilDate(int year, int month, int dayOfMonth) {
        return Date.from(LocalDate.of(year, month, dayOfMonth).atStartOfDay().toInstant(ZoneOffset.UTC));
    }

    private static void addAscentWithRoute(String routeName, String wallName, String cragName,
                                           String difficulty, Status status, Date date) {
        RouteDto routeDto = new RouteDto(routeName, wallName, cragName);
        AscentDto ascentDto = new AscentDto(routeDto, difficulty, status, date);
        ResponseEntity<AscentDto> response = wspinClient.createAscent(ascentDto, USER_TOKEN_1);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotEmpty();
        assertThat(response.getBody().getRouteDto().getId()).isNotEmpty();
        ASCENT_IDS.add(response.getBody().getId());
        ROUTE_IDS.add(response.getBody().getRouteDto().getId());
    }
}
