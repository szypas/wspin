import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import szymek.wspin.WspinClient;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;
import szymek.wspin.util.Status;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AscentsControllerTest {
    private static final String USER_NAME_1 = "newUser";
    private static final String USER_NAME_2 = "anotherNewUser";
    private static final String USER_EMAIL_1 = "user@email";
    private static final String USER_EMAIL_2 = "anotheruser@email";
    private static final String USER_PASS_1 = "123u";
    private static final String USER_PASS_2 = "123au";
    private static final String ROUTE_NAME = "myRoute";
    private static final String WALL_NAME = "myWall";
    private static final String CRAG_NAME = "myCrag";
    private static String USER_TOKEN_1;
    private static String USER_TOKEN_2;
    private static String ASCENT_ID;
    private static String ROUTE_ID;
    private static final WspinClient wspinClient = new WspinClient();

    @BeforeAll
     static void setup() {
        wspinClient.registerUser(new UserDto(USER_NAME_1, USER_EMAIL_1, USER_PASS_1));
        wspinClient.registerUser(new UserDto(USER_NAME_2, USER_EMAIL_2, USER_PASS_2));
        USER_TOKEN_1 = Objects.requireNonNull(wspinClient.login(USER_NAME_1, USER_PASS_1).getBody()).getToken();
        USER_TOKEN_2 = Objects.requireNonNull(wspinClient.login(USER_NAME_2, USER_PASS_2).getBody()).getToken();

    }

    @AfterAll
    static void cleanup() {
        wspinClient.deleteRoute(ROUTE_ID, USER_TOKEN_1);
        wspinClient.deleteUser(USER_TOKEN_1);
        wspinClient.deleteUser(USER_TOKEN_2);
    }

    @Test
    @Order(10)
    void anonymousCantGetAscents() {
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.getAscents(null));
    }

    @Test
    @Order(20)
    void anonymousCantPostAscent() {
        RouteDto routeDto = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        AscentDto ascentDto = new AscentDto(routeDto, "V", Status.OS, Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.createAscent(ascentDto, null));
    }

    @Test
    @Order(30)
    void userCanPostAscent() {
        RouteDto routeDto = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        AscentDto ascentDto = new AscentDto(routeDto, "V", Status.OS, Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        ResponseEntity<AscentDto> result = wspinClient.createAscent(ascentDto, USER_TOKEN_1);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody().toString()).contains(ROUTE_NAME, WALL_NAME, CRAG_NAME, "OS", "V"); // todo add date
        System.out.println(result.getBody());
        ASCENT_ID = result.getBody().getId();
        ROUTE_ID = result.getBody().getRouteDto().getId();
    }

    @Test
    @Order(40)
    void userCantPostDuplicatedAscent() {
        RouteDto routeDto = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
        AscentDto ascentDto = new AscentDto(routeDto, "V", Status.OS, Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        assertThrows(HttpClientErrorException.Conflict.class, () -> wspinClient.createAscent(ascentDto, USER_TOKEN_1));
    }

    @Test
    @Order(50)
    void anonymousCantDeleteAscent() {
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.deleteAscent(ASCENT_ID, null));
    }

    @Test
    @Order(60)
    void userCanGetOwnAscent() {
        ResponseEntity<List> result = wspinClient.getAscents(USER_TOKEN_1);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).isNotEmpty();
        assertThat(result.getBody().toString()).contains(ROUTE_NAME, WALL_NAME, CRAG_NAME, "OS", "V"); // todo add date
    }

    @Test
    @Order(70)
    void userCantGetOtherAscent() {
        ResponseEntity<List> result = wspinClient.getAscents(USER_TOKEN_2);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).isEmpty();
    }

    @Test
    @Order(80)
    void userCantDeleteOtherUserAscent() {
        assertThrows(HttpClientErrorException.Forbidden.class, () -> wspinClient.deleteAscent(ASCENT_ID, USER_TOKEN_2));
    }

    @Test
    @Order(90)
    void userCanDeleteOwnAscents() {
        wspinClient.deleteAscent(ASCENT_ID, USER_TOKEN_1);
    }

    @Test
    @Order(100)
    void makeSureUserDeletedOwnAscent() {
        ResponseEntity<List> result = wspinClient.getAscents(USER_TOKEN_1);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).isEmpty();
    }
}
