import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import szymek.wspin.WspinClient;
import szymek.wspin.dto.JwtResponse;
import szymek.wspin.dto.UserDto;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RegisterAuthAdminControllerTest {
    private static final String USER_NAME = "newUser";
    private static final String ADMIN_NAME = "newAdmin";
    private static final String USER_EMAIL = "user@email";
    private static final String ADMIN_EMAIL = "admin@email";
    private static final String USER_PASS = "123u";
    private static final String ADMIN_PASS = "123a";
    private static String USER_TOKEN;
    private static String ADMIN_TOKEN;
    private final WspinClient wspinClient = new WspinClient();

    @Test
    @Order(10)
    void registerUser_loginAsUser_createAdmin_loginAsAdmin_getUsersByAdmin_removeAdmin_removeUser() {
        UserDto userDto = new UserDto(USER_NAME, USER_EMAIL, USER_PASS);
        ResponseEntity<UserDto> response = wspinClient.registerUser(userDto);
        assertUserWasRegisteredCorrectly(userDto, response);
    }

    @Test
    @Order(20)
    void loginAsUser() {
        ResponseEntity<JwtResponse> loginResponse = wspinClient.login(USER_NAME, USER_PASS);
        assertUserLoggedInCorrectly(loginResponse);
        USER_TOKEN = Objects.requireNonNull(loginResponse.getBody()).getToken();
    }

    @Test
    @Order(30)
    void registerAdmin() {
        UserDto userDto = new UserDto(ADMIN_NAME, ADMIN_EMAIL, ADMIN_PASS);
        ResponseEntity<UserDto> response = wspinClient.registerAdmin(userDto, USER_TOKEN);
        assertUserWasRegisteredCorrectly(userDto, response);
        assertThat(Objects.requireNonNull(response.getBody()).getRoles()).contains("ROLE_ADMIN");
    }

    @Test
    @Order(40)
    void loginAsAdmin() {
        ResponseEntity<JwtResponse> loginResponse = wspinClient.login(ADMIN_NAME, ADMIN_PASS);
        assertUserLoggedInCorrectly(loginResponse);
        ADMIN_TOKEN = Objects.requireNonNull(loginResponse.getBody()).getToken();
    }

    @Test
    @Order(50)
    void getListOfUsersAsAdmin() {
        ResponseEntity<List> users = wspinClient.getAllUsers(ADMIN_TOKEN);
        assertThat(users.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(users.getBody()).isNotNull();
        assertThat(users.getBody()).hasSize(2);
        assertThat(users.getBody().toString()).contains("newUser");
        assertThat(users.getBody().toString()).contains("newAdmin");
    }

    @Test
    @Order(60)
    void removeUserAndAdmin() {
        wspinClient.deleteUser(USER_TOKEN);
        wspinClient.deleteUser(ADMIN_TOKEN);
    }

    @Test
    @Order(70)
    void makeSureAdminAndUserAreRemoevd() {
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.login(USER_NAME, USER_PASS));
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.login(ADMIN_NAME, ADMIN_PASS));
    }

    @Test
    void userCantGetUsersList() {
        UserDto userDto = new UserDto(ADMIN_NAME, ADMIN_EMAIL, ADMIN_PASS);
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.registerAdmin(userDto, null));
    }

    @Test
    void anonymousCantRegisterAdmin() {
        assertThrows(HttpClientErrorException.Unauthorized.class, () -> wspinClient.getAllUsers(null));
    }

    private void assertUserLoggedInCorrectly(ResponseEntity<JwtResponse> loginResponse) {
        assertThat(loginResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(loginResponse.getBody()).isNotNull();
        assertThat(loginResponse.getBody().getToken()).isNotEmpty();
    }

    private void assertUserWasRegisteredCorrectly(UserDto userDto, ResponseEntity<UserDto> response) {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        UserDto responseBody = response.getBody();
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getLogin()).isEqualTo(userDto.getLogin());
        assertThat(responseBody.getEmail()).isEqualTo(userDto.getEmail());
        assertThat(responseBody.getPassword()).isNotEmpty();
        assertThat(responseBody.getRoles()).contains("ROLE_USER");
    }


}
