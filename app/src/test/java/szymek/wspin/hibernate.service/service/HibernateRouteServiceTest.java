package szymek.wspin.hibernate.service.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.repository.CragRepository;
import szymek.wspin.hibernate.repository.RouteRepository;
import szymek.wspin.hibernate.repository.WallRepository;
import szymek.wspin.hibernate.service.HibernateRouteService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class HibernateRouteServiceTest {
    private final RouteRepository routeRepository = Mockito.mock(RouteRepository.class);
    private final WallRepository wallRepository = Mockito.mock(WallRepository.class);
    private final CragRepository cragRepository = Mockito.mock(CragRepository.class);
    private final Crag CRAG_1 = new Crag("crag1");
    private final Wall WALL_1 = new Wall("wall1", CRAG_1);
    private final Route ROUTE_1 = new Route("route1", WALL_1);
    private final Route ROUTE_2 = new Route("route2", new Wall("wall2", new Crag("crag2")));
    private final List<Route> ROUTES = new ArrayList<>(Arrays.asList(ROUTE_1, ROUTE_2));
    private final RouteDto ROUTE_1_DTO = new RouteDto("route1", "wall1", "crag1");
    private final HibernateRouteService routeService = new HibernateRouteService(routeRepository, wallRepository, cragRepository);

    @Test
    void testGetAllRoutes() {
        when(routeRepository.findAll()).thenReturn(ROUTES);
        Assertions.assertEquals(ROUTES, routeService.getAllRoutes());
    }

    @Test
    void testGetOneRoute() {
        when(routeRepository.findByNameAndWallNameAndWallCragName(
                ROUTE_1_DTO.getRouteName(), ROUTE_1_DTO.getWallName(), ROUTE_1_DTO.getCragName()
        )).thenReturn(ROUTE_1);
        Assertions.assertEquals(ROUTE_1, routeService.getRoute(ROUTE_1_DTO));
    }

    @Test
    void testAddRoute_wallAndCragAlreadyInDatabase() {
        when(cragRepository.findByName(ROUTE_1_DTO.getCragName())).thenReturn(CRAG_1);
        when(wallRepository.findByNameAndCragName(ROUTE_1_DTO.getWallName(), ROUTE_1_DTO.getCragName())).thenReturn(WALL_1);
        routeService.addRoute(ROUTE_1_DTO);
        verify(routeRepository, times(1)).save(ROUTE_1);
    }

    @Test
    void testAddRoute_createNewWallInExistingCrag() {
        when(cragRepository.findByName(ROUTE_1_DTO.getCragName())).thenReturn(CRAG_1);
        when(wallRepository.findByNameAndCragName(ROUTE_1_DTO.getWallName(), ROUTE_1_DTO.getCragName())).thenReturn(null);
        routeService.addRoute(ROUTE_1_DTO);
        verify(routeRepository, times(1)).save(isA(Route.class));
        verify(routeRepository, times(1)).save(argThat(route -> route.getWall() != null));
    }

    @Test
    void testAddRoute_createNewWallAndCrag() {
        when(cragRepository.findByName(ROUTE_1_DTO.getCragName())).thenReturn(null);
        when(wallRepository.findByNameAndCragName(ROUTE_1_DTO.getWallName(), ROUTE_1_DTO.getCragName())).thenReturn(null);
        routeService.addRoute(ROUTE_1_DTO);
        verify(routeRepository, times(1)).save(isA(Route.class));
        verify(routeRepository, times(1)).save(argThat(route -> route.getWall() != null));
    }

    @Test
    void testDeleteRouteById() {
        Long routeId = 7L;
        routeService.deleteRoute(routeId);
        verify(routeRepository, times(1)).deleteById(routeId);
    }
}
