package szymek.wspin.hibernate.service.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.repository.ascentRepository.AscentRepository;
import szymek.wspin.hibernate.service.HibernateAscentService;
import szymek.wspin.hibernate.service.HibernateRouteService;
import szymek.wspin.hibernate.service.HibernateUserService;
import szymek.wspin.util.AscentSearchFilter;
import szymek.wspin.util.Status;

import javax.persistence.EntityExistsException;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class HibernateAscentServiceTest {
    private final AscentRepository ascentRepository = Mockito.mock(AscentRepository.class);
    private final HibernateUserService userService = Mockito.mock(HibernateUserService.class);
    private final HibernateRouteService routeService = Mockito.mock(HibernateRouteService.class);
    private final String CRAG_NAME = "crag1";
    private final String WALL_NAME = "wall1";
    private final String ROUTE_NAME = "route1";
    private final String DIFFICULTY = "VI.3";
    private final AscentSearchFilter FILTER = new AscentSearchFilter();
    private final Crag CRAG = new Crag(CRAG_NAME);
    private final Wall WALL = new Wall(WALL_NAME, CRAG);
    private final Route ROUTE = new Route(ROUTE_NAME, WALL);
    private final User USER = new User("name", "em@ai.l", "password");
    private final Ascent ASCENT = new Ascent(DIFFICULTY, Status.RP, ROUTE, USER, new java.sql.Date(0));
    private final RouteDto ROUTE_DTO = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME);
    private final AscentDto ASCENT_DTO = new AscentDto(ROUTE_DTO, DIFFICULTY, Status.RP, new java.util.Date(0));
    private final HibernateAscentService ascentService = new HibernateAscentService(ascentRepository, userService, routeService);

    @Test
    void testGetAscents() {
        when(ascentRepository.findByUser(USER, FILTER)).thenReturn(List.of(ASCENT));
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        List<Ascent> result = ascentService.getAscents(USER.getLogin(), FILTER);
        assertThat(result).hasSize(1).contains(ASCENT);
    }

    @Test
    void testDeleteAscentByIdAndUser() {
        long idToBeRemoved = 7;
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        ascentService.removeAscent(idToBeRemoved, USER.getLogin());
        verify(ascentRepository).deleteByIdAndUser(idToBeRemoved, USER);
    }

    @Test
    void whenAscentAlreadyExits_returnUnprocessableEntity() {
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(ROUTE);
        when(ascentRepository.findByRouteAndUser(ROUTE, USER)).thenReturn(ASCENT);
        assertThrows(EntityExistsException.class, () -> ascentService.addAscent(ASCENT_DTO, USER.getLogin()));
    }

    @Test
    void whenRouteAlreadyExists_useExistingRoute_andAddAscent() {
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(ROUTE);
        when(ascentRepository.findByRouteAndUser(ROUTE, USER)).thenReturn(null);
        assertThat(ascentService.addAscent(ASCENT_DTO, USER.getLogin())).isEqualTo(ASCENT);
        verify(ascentRepository).save(any());
    }

    @Test
    void whenRouteDoesntExist_createNewRoute_andAddAscent() {
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(null);
        when(routeService.addRoute(ROUTE_DTO)).thenReturn(ROUTE);
        assertThat(ascentService.addAscent(ASCENT_DTO, USER.getLogin())).isEqualTo(ASCENT);
        verify(ascentRepository).save(any());
    }

    @Test
    void whenDateInAscentDtoIsNull_dontThrow() {
        ASCENT_DTO.setDate(null);
        ASCENT.setDate(null);
        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(null);
        when(routeService.addRoute(ROUTE_DTO)).thenReturn(ROUTE);
        assertThat(ascentService.addAscent(ASCENT_DTO, USER.getLogin())).isEqualTo(ASCENT);
        verify(ascentRepository).save(any());
    }
}
