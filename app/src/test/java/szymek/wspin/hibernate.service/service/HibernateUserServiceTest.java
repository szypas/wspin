package szymek.wspin.hibernate.service.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.repository.UserRepository;
import szymek.wspin.hibernate.service.HibernateUserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class HibernateUserServiceTest {
    private final UserDto USER_DTO = new UserDto("szymon", "szymon@email", "321");
    private final List<Role> ADMIN_ROLES = Arrays.asList(new Role("ROLE_USER"), new Role("ADMIN_ROLE"));
    private final List<Role> USER_ROLES = Collections.singletonList(new Role("ROLE_USER"));
    private final UserRepository USER_REPOSITORY = Mockito.mock(UserRepository.class);
    private final BCryptPasswordEncoder PASSWORD_ENCODER = Mockito.mock(BCryptPasswordEncoder.class);
    private final User USER = new User("szymon", "szymon@email", "encoded321");
    private final HibernateUserService userService = new HibernateUserService(USER_REPOSITORY, PASSWORD_ENCODER);

    @Test
    void saveUserWithRoleUser() {
        USER.setRoles(USER_ROLES);
        when(PASSWORD_ENCODER.encode(USER_DTO.getPassword())).thenReturn(USER.getPassword());
        userService.save(USER_DTO);
        verify(PASSWORD_ENCODER).encode(USER_DTO.getPassword());
        verify(USER_REPOSITORY).save(USER);
    }

    @Test
    void saveUserWithRolesUserAndAdmin() {
        USER.setRoles(ADMIN_ROLES);
        when(PASSWORD_ENCODER.encode("321")).thenReturn("encoded321");
        userService.save(USER_DTO, ADMIN_ROLES);
        verify(PASSWORD_ENCODER).encode("321");
        verify(USER_REPOSITORY).save(USER);
    }

    @Test
    void getUserByName() {
        userService.getByName("name");
        verify(USER_REPOSITORY).findByLogin("name");
    }

    @Test
    void loadByUsername_userExists() {
        USER.setRoles(USER_ROLES);
        UserDetails expectedUserDetails = new org.springframework.security.core.userdetails.User(
                USER.getLogin(),
                USER.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
        );
        when(USER_REPOSITORY.findByLogin(USER.getLogin())).thenReturn(USER);
        assertThat(userService.loadUserByUsername(USER.getLogin())).isEqualTo(expectedUserDetails);
    }

    @Test
    void loadByUsername_userNotFound() {
        when(USER_REPOSITORY.findByLogin("name")).thenReturn(null);
        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("name"));
    }

    @Test
    void getAllUsers() {
        when(USER_REPOSITORY.findAll()).thenReturn(List.of(USER, USER));
        assertThat(userService.getAllUsers()).isEqualTo(List.of(USER, USER));
    }
}
