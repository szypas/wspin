package szymek.wspin.adapters;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.service.HibernateUserService;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HibernateUserAdapterTest {
    private static final String USER_NAME = "login1";
    private static final String PASSWORD = "password1";
    private static final String EMAIL = "email1";
    private static final User USER = new User(USER_NAME, EMAIL, PASSWORD);
    private static final List<Role> ROLES = List.of(new Role("role"));
    private static final List<String> ROLES_AS_STRINGS = List.of("role");
    private static final User USER_WITH_ROLES = new User(USER_NAME, EMAIL, PASSWORD, ROLES);
    private static final UserDto USER_DTO = new UserDto(USER_NAME, EMAIL, PASSWORD);
    private static final UserDto USER_WITH_ROLES_DTO = new UserDto(USER_NAME, EMAIL, PASSWORD, ROLES_AS_STRINGS);
    private static final UserDetails USER_DETAILS = mock(UserDetails.class);
    private final HibernateUserService userService = mock(HibernateUserService.class);
    private final HibernateUserAdapter userAdapter = new HibernateUserAdapter(userService);

    @Test
    void save_whenValidUser_thenReturnUserDto() {
        when(userService.save(USER_DTO)).thenReturn(USER);
        assertThat(userAdapter.save(USER_DTO)).isEqualTo(USER_DTO);
    }

    @Test
    void save_whenNullUser_thenReturnNull() {
        when(userService.save(USER_DTO)).thenReturn(null);
        assertThat(userAdapter.save(USER_DTO)).isEqualTo(null);
    }

    @Test
    void save_whenUserWithRoles_thenReturnUserDtoWithRoles() {
        when(userService.save(USER_DTO, ROLES)).thenReturn(USER_WITH_ROLES);
        assertThat(userAdapter.save(USER_DTO, ROLES_AS_STRINGS)).isEqualTo(USER_WITH_ROLES_DTO);
    }

    @Test
    void getByName() {
        when(userService.getByName(USER_NAME)).thenReturn(USER);
        assertThat(userAdapter.getByName(USER_NAME)).isEqualTo(USER_DTO);
    }

    @Test
    void loadUserByUsername() {
        when(userService.loadUserByUsername(USER_NAME)).thenReturn(USER_DETAILS);
        assertThat(userAdapter.loadUserByUsername(USER_NAME)).isEqualTo(USER_DETAILS);
    }

    @Test
    void getAllUsers() {
        when(userService.getAllUsers()).thenReturn(List.of(USER, USER, USER));
        assertThat(userAdapter.getAllUsers()).isEqualTo(List.of(USER_DTO, USER_DTO, USER_DTO));
    }
}
