package szymek.wspin.adapters;

import org.junit.jupiter.api.Test;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.service.HibernateRouteService;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HibernateRouteAdapterTest {
    private static final String CRAG_NAME = "crag1";
    private static final String WALL_NAME = "wall1";
    private static final String ROUTE_NAME = "route1";
    private static final String ROUTE_ID = "7";
    private static final Crag CRAG = new Crag(CRAG_NAME);
    private static final Wall WALL = new Wall(WALL_NAME, CRAG);
    private static final Route ROUTE = new Route(Long.valueOf(ROUTE_ID), ROUTE_NAME, WALL);
    private static final RouteDto ROUTE_DTO = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME, ROUTE_ID);
    private final HibernateRouteService routeService = mock(HibernateRouteService.class);
    private final HibernateRouteAdapter routeAdapter = new HibernateRouteAdapter(routeService);

    @Test
    void deleteRoute_shouldDeleteRoute() {
        routeAdapter.deleteRoute(ROUTE_ID);
        verify(routeService).deleteRoute(Long.valueOf(ROUTE_ID));
    }

    @Test
    void getRoute_whenRouteIsValid_thenReturnRouteDto() {
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(ROUTE);
        assertThat(routeAdapter.getRoute(ROUTE_DTO)).isEqualTo(ROUTE_DTO);
    }

    @Test
    void getRoute_whenMultipleRoutes_thenReturnMultipleRouteDto() {
        when(routeService.getAllRoutes()).thenReturn(List.of(ROUTE, ROUTE));
        assertThat(routeAdapter.getAllRoutes()).isEqualTo(List.of(ROUTE_DTO, ROUTE_DTO));
    }

    @Test
    void getRoute_whenRouteIsNull_thenReturnNull() {
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(null);
        assertThat(routeAdapter.getRoute(ROUTE_DTO)).isEqualTo(null);
    }

    @Test
    void addRoute_whenAddingRoute_RouteDtoShouldBeReturned() {
        when(routeService.addRoute(ROUTE_DTO)).thenReturn(ROUTE);
        assertThat(routeAdapter.addRoute(ROUTE_DTO)).isEqualTo(ROUTE_DTO);
    }

}
