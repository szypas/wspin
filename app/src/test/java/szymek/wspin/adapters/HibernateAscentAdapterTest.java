package szymek.wspin.adapters;

import org.junit.jupiter.api.Test;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.service.HibernateAscentService;
import szymek.wspin.util.AscentSearchFilter;
import szymek.wspin.util.Status;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class HibernateAscentAdapterTest {
    private static final String USER_NAME = "login1";
    private static final String CRAG_NAME = "crag1";
    private static final String WALL_NAME = "wall1";
    private static final String ROUTE_NAME = "route1";
    private static final String PASSWORD = "password1";
    private static final String EMAIL = "email1";
    private static final Long ROUTE_ID = 7L;
    private static final Long ASCENT_ID = 8L;
    private static final String DIFFICULTY = "VI.4";
    private static final Crag CRAG = new Crag(CRAG_NAME);
    private static final Wall WALL = new Wall(WALL_NAME, CRAG);
    private static final Route ROUTE = new Route(ROUTE_ID, ROUTE_NAME, WALL);
    private static final User USER = new User(USER_NAME, EMAIL, PASSWORD);
    private static final Date DATE = Date.valueOf(LocalDate.of(2022, 2, 2));
    private static final Ascent ASCENT = new Ascent(ASCENT_ID, DIFFICULTY, Status.OS, ROUTE, USER, DATE);
    private static final RouteDto ROUTE_DTO = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME, ROUTE_ID.toString());
    private static final AscentDto ASCENT_DTO = new AscentDto(ROUTE_DTO, DIFFICULTY, Status.OS, DATE, ASCENT_ID.toString());
    private static final AscentSearchFilter FILTER = new AscentSearchFilter();
    private final HibernateAscentService ascentService = mock(HibernateAscentService.class);
    private final HibernateAscentAdapter ascentAdapter = new HibernateAscentAdapter(ascentService);

    @Test
    void addAscent_whenAscentNotNull_ThenReturnCorrectAscentDto() {
        when(ascentService.addAscent(ASCENT_DTO, USER_NAME)).thenReturn(ASCENT);
        assertThat(ascentAdapter.addAscent(ASCENT_DTO, USER_NAME)).isEqualTo(ASCENT_DTO);
    }

    @Test
    void addAscent_whenAscentIsNull_ThenReturnNull() {
        when(ascentService.addAscent(ASCENT_DTO, USER_NAME)).thenReturn(null);
        assertThat(ascentAdapter.addAscent(ASCENT_DTO, USER_NAME)).isEqualTo(null);
    }

    @Test
    void getAscents_whenAscentIsNull_ThenReturnNull() {
        when(ascentService.getAscents(USER_NAME, FILTER)).thenReturn(List.of(ASCENT, ASCENT));
        assertThat(ascentAdapter.getAscents(USER_NAME, FILTER)).isEqualTo(List.of(ASCENT_DTO, ASCENT_DTO));
    }

    @Test
    void removeAscent_shouldRemoveAscent() {
        ascentAdapter.removeAscent(ASCENT_ID.toString(), USER_NAME);
        verify(ascentService).removeAscent(ASCENT_ID, USER_NAME);
    }
}
