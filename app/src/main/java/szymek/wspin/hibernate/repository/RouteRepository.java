package szymek.wspin.hibernate.repository;

import org.springframework.data.repository.CrudRepository;
import szymek.wspin.hibernate.model.Route;

public interface RouteRepository extends CrudRepository<Route, Long> {
    Route findByNameAndWallNameAndWallCragName(String routeName, String wallName, String cragName);
    void deleteById(Long id);
}
