package szymek.wspin.hibernate.repository.ascentRepository;

import org.springframework.data.repository.CrudRepository;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.User;

public interface AscentRepository extends CrudRepository<Ascent, Long>, CustomAscentRepository {
    Ascent findByRouteAndUser(Route route, User user);
    long deleteByIdAndUser(Long id, User user);
    void deleteById(Long id);
}
