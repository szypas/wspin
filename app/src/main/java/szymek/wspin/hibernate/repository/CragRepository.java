package szymek.wspin.hibernate.repository;

import org.springframework.data.repository.CrudRepository;
import szymek.wspin.hibernate.model.Crag;

public interface CragRepository extends CrudRepository<Crag, Long> {
    Crag findByName(String cragName);
}
