package szymek.wspin.hibernate.repository.ascentRepository;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.util.AscentSearchFilter;
import szymek.wspin.util.Status;
import szymek.wspin.util.difficulty.DifficultyUtil;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class CustomAscentRepositoryImpl implements CustomAscentRepository {
    @Autowired
    EntityManager entityManager;

    @Override
    public List<Ascent> findByUser(User user, AscentSearchFilter filter) {
        // all 3 methods are used only in order to learn them
        List<Ascent> filterResult = filters(user, filter);
        List<Ascent> criteriaResult = criteria(user, filter);
        List<Ascent> hqlResult = hql(user, filter);
        compare(filterResult, criteriaResult, hqlResult);
        return hqlResult;
    }

    private void compare(List<Ascent> filterResult, List<Ascent> criteriaResult, List<Ascent> hqlResult) {
        if (filterResult.containsAll(criteriaResult)
                && filterResult.containsAll(hqlResult)
                && criteriaResult.containsAll(filterResult)
                && criteriaResult.containsAll(hqlResult)
                && hqlResult.containsAll(filterResult)
                && hqlResult.containsAll(criteriaResult)) {
            return;
        }
        log.error("filterResult: " + filterResult);
        log.error("criteriaResult: " + criteriaResult);
        log.error("hqlResult: " + hqlResult);
        throw new RuntimeException("discrepancy in getAscent");
    }

    private List<Ascent> hql(User user, AscentSearchFilter filter) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder query = new StringBuilder("from Ascent ");
        query.append(" where user_id='" + user.getId() + "' ");
        query.append(" and status in (" + getSqlFormatForCollection(filter.getStatuses()) + ") ");
        List<String> diffs = DifficultyUtil.generateListOfDifficulties(filter.getDifficultyFrom(), filter.getDifficultyTo());
        query.append(" and difficulty in (" + getSqlFormatForCollection(diffs) + ") ");
        if (filter.getDateFrom() == null && filter.getDateTo() == null) {
            log.debug("skip date filter");
        } else {
            query.append(" and date >= '" + dateFormat.format(filter.getDateFrom()) + "'");
            query.append(" and date <= '" + dateFormat.format(filter.getDateTo()) + "'");
        }
        return (List<Ascent>)entityManager.createQuery(query.toString()).getResultList();
    }

    private String getSqlFormatForCollection(Set<Status> statuses) {
        StringBuilder sb = new StringBuilder("'");
        for (Status status : statuses) {
            sb.append(status).append("', '");
        }
        return sb.substring(0, sb.length() - 3);
    }

    private String getSqlFormatForCollection(List<String> difficulties) {
        StringBuilder sb = new StringBuilder("'");
        for (String difficulty : difficulties) {
            sb.append(difficulty).append("', '");
        }
        return sb.substring(0, sb.length() - 3);
    }

    private List<Ascent> filters(User user, AscentSearchFilter filter) {
        Session session = entityManager.unwrap(Session.class);
        session.enableFilter("ascentUserFilter").setParameter("userId", user.getId());
        if (filter.getDateFrom() == null && filter.getDateTo() == null) {
            log.debug("skip date filter");
        } else {
            session.enableFilter("ascentDateFilter")
                    .setParameter("fromDate", filter.getDateFrom())
                    .setParameter("toDate", filter.getDateTo());
        }
        session.enableFilter("ascentDifficultyFilter").setParameterList(
                "difficulties",
                DifficultyUtil.generateListOfDifficulties(filter.getDifficultyFrom(), filter.getDifficultyTo()));
        session.enableFilter("ascentStatusFilter").setParameterList(
                "statuses",
                filter.getStatuses().stream().map(Enum::toString).collect(Collectors.toList()));
        List<Ascent> result = session.createQuery("from Ascent ").getResultList();
        session.disableFilter("ascentStatusFilter");
        session.disableFilter("ascentDifficultyFilter");
        session.disableFilter("ascentDateFilter");
        session.disableFilter("ascentUserFilter");
        return result;
    }

    private List<Ascent> criteria(User user, AscentSearchFilter filter) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Ascent> query = criteriaBuilder.createQuery(Ascent.class);
        Root<Ascent> ascent = query.from(Ascent.class);
        Join<Ascent, User> joinedUser = ascent.join("user");
        List<String> difficulties = DifficultyUtil.generateListOfDifficulties(filter.getDifficultyFrom(), filter.getDifficultyTo());
        Predicate[] predicates;
        if (filter.getDateFrom() == null && filter.getDateTo() == null) {
            predicates = new Predicate[3];
            log.debug("skip date filter");
        } else {
            predicates = new Predicate[4];
            predicates[3] = criteriaBuilder.between(ascent.<Date>get("date"), filter.getDateFrom(), filter.getDateTo());
        }
        predicates[0] = criteriaBuilder.equal(joinedUser.get("id"), user.getId());
        predicates[1] = ascent.get("difficulty").in(difficulties);
        predicates[2] = ascent.get("status").in(List.of(filter.getStatuses()));
        query.select(ascent).where(predicates);
        return entityManager.createQuery(query).getResultList();
    }
}
