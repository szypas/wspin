package szymek.wspin.hibernate.repository;

import org.springframework.data.repository.CrudRepository;
import szymek.wspin.hibernate.model.Wall;

public interface WallRepository extends CrudRepository<Wall, Long> {
    Wall findByNameAndCragName(String wallName, String cragName);
}
