package szymek.wspin.hibernate.repository.ascentRepository;

import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.util.AscentSearchFilter;

import java.util.List;

public interface CustomAscentRepository {
    List<Ascent> findByUser(User user, AscentSearchFilter filter);
}
