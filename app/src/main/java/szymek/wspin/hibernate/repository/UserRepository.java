package szymek.wspin.hibernate.repository;

import org.springframework.data.repository.CrudRepository;
import szymek.wspin.hibernate.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByLogin(String s);

    void deleteByLogin(String name);
}
