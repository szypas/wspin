package szymek.wspin.hibernate.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Route {
    @Id
    @GeneratedValue
    Long id;
    String name;
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST})
    Wall wall;

    public Route(String name, Wall wall) {
        this.name = name;
        this.wall = wall;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, wall);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route)o;
        return name.equals(route.name) &&
                wall.equals(route.wall);
    }
}
