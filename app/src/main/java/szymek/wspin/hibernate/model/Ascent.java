package szymek.wspin.hibernate.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import szymek.wspin.util.Status;

import javax.persistence.*;
import java.sql.Date;

@FilterDef(name = "ascentStatusFilter", parameters = {@ParamDef(name = "statuses", type = "string")})
@Filter(name = "ascentStatusFilter", condition = "status in (:statuses)")

@FilterDef(name = "ascentDateFilter", parameters = {
        @ParamDef(name = "fromDate", type = "java.sql.Date"),
        @ParamDef(name = "toDate", type = "java.sql.Date")})
@Filter(name = "ascentDateFilter", condition = "date > :fromDate and date < :toDate")

@FilterDef(name = "ascentDifficultyFilter", parameters = {@ParamDef(name = "difficulties", type = "string")})
@Filter(name = "ascentDifficultyFilter", condition = "difficulty in (:difficulties)")

@FilterDef(name = "ascentUserFilter", parameters = @ParamDef(name = "userId", type = "long"))
@Filter(name = "ascentUserFilter", condition = "user_id = :userId")

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Ascent {
    @Id
    @GeneratedValue
    private Long id;
    private String difficulty;
    private Status status;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Route route;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @Filter(name = "ascentUserFilter", condition = "user_id = :userId")
    private User user;

    private Date date;

    public Ascent(String difficulty, Status status, Route route, User user, Date date) {
        this.difficulty = difficulty;
        this.status = status;
        this.route = route;
        this.user = user;
        this.date = date;
    }
}
