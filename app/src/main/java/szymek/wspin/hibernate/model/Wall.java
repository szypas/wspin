package szymek.wspin.hibernate.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
public class Wall {
    @Id
    @GeneratedValue
    Long id;
    String name;
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST})
    Crag crag;

    public Wall(String name, Crag crag) {
        this.name = name;
        this.crag = crag;
    }

    public Wall() {
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wall wall = (Wall)o;
        return name.equals(wall.name);
    }
}
