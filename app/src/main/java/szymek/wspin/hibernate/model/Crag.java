package szymek.wspin.hibernate.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Crag {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    public Crag() {
    }

    public Crag(String name) {
        this.name = name;
    }
}
