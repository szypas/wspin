package szymek.wspin.hibernate.service;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Profile({"h2", "rds"})
@Service
@AllArgsConstructor
public class HibernateUserService implements UserDetailsService {
    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final BCryptPasswordEncoder passwordEncoder;

    public User save(UserDto userDto) {
        User newUser = new User(
                userDto.getLogin(),
                userDto.getEmail(),
                passwordEncoder.encode(userDto.getPassword()),
                Collections.singletonList(new Role("ROLE_USER")));
        userRepository.save(newUser);
        return newUser;
    }

    public User save(UserDto userDto, List<Role> roles) {
        User newUser = new User(
                userDto.getLogin(),
                userDto.getEmail(),
                passwordEncoder.encode(userDto.getPassword()),
                roles);
        userRepository.save(newUser);
        return newUser;
    }

    public User getByName(String name) {
        return userRepository.findByLogin(name);
    }

    public List<User> getAllUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteByLogin(String name) {
        userRepository.deleteByLogin(name);
    }
}
