package szymek.wspin.hibernate.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.repository.ascentRepository.AscentRepository;
import szymek.wspin.util.AscentSearchFilter;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Slf4j
@Component
@Profile({"h2", "rds"})
@AllArgsConstructor
public class HibernateAscentService {
    private final AscentRepository ascentRepository;
    private final HibernateUserService userService;
    private final HibernateRouteService routeService;

    private Ascent addAscent(AscentDto ascentDto, Route route, User user) {
        Ascent ascent = new Ascent();
        ascent.setRoute(route);
        ascent.setDifficulty(ascentDto.getDifficulty());
        ascent.setStatus(ascentDto.getStatus());
        ascent.setUser(user);
        ascent.setDate(ascentDto.getDate() == null ? null : new Date(ascentDto.getDate().toInstant().toEpochMilli()));
        ascentRepository.save(ascent);
        return ascent;
    }

    public Ascent addAscent(AscentDto ascentDto, String userName) {
        Ascent ascent;
        User user = userService.getByName(userName);
        Route route = routeService.getRoute(ascentDto.getRouteDto());
        if (route != null) {
            ascent = ascentRepository.findByRouteAndUser(route, user);
            if (ascent != null) {
                throw new EntityExistsException();
            }
        } else {
            route = routeService.addRoute(ascentDto.getRouteDto());
        }
        return addAscent(ascentDto, route, user);
    }

    public List<Ascent> getAscents(String userName, AscentSearchFilter filter) {
        User user = userService.getByName(userName);
        List<Ascent> userAscents = ascentRepository.findByUser(user, filter);
        log.debug("ascents: " + userAscents);
        return userAscents;
    }

    @Transactional
    public void removeAscent(Long id, String userName) {
        User user = userService.getByName(userName);
        ascentRepository.deleteByIdAndUser(id, user);
    }
}
