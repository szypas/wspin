package szymek.wspin.hibernate.service;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.repository.CragRepository;
import szymek.wspin.hibernate.repository.RouteRepository;
import szymek.wspin.hibernate.repository.WallRepository;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Profile({"h2", "rds"})
@AllArgsConstructor
public class HibernateRouteService {
    private final RouteRepository routeRepository;
    private final WallRepository wallRepository;
    private final CragRepository cragRepository;

    public List<Route> getAllRoutes() {
        return Lists.newArrayList(routeRepository.findAll());
    }

    @Transactional
    public Route addRoute(RouteDto routeDto) {
        Wall wall;
        Crag crag = cragRepository.findByName(routeDto.getCragName());
        if (crag == null) {
            crag = createCrag(routeDto.getCragName());
            wall = createWall(routeDto.getWallName(), crag);
        } else {
            wall = getOrCreateWall(routeDto, crag);
        }
        return saveRoute(routeDto, wall);
    }

    private Wall getOrCreateWall(RouteDto routeDto, Crag crag) {
        Wall wall = wallRepository.findByNameAndCragName(routeDto.getWallName(), routeDto.getCragName());
        if (wall == null) {
            wall = createWall(routeDto.getWallName(), crag);
        }
        return wall;
    }

    private Route saveRoute(RouteDto routeDto, Wall wall) {
        Route route = new Route();
        route.setName(routeDto.getRouteName());
        route.setWall(wall);
        routeRepository.save(route);
        return route;
    }

    public Route getRoute(RouteDto routeDto) {
        return routeRepository.findByNameAndWallNameAndWallCragName(
                routeDto.getRouteName(),
                routeDto.getWallName(),
                routeDto.getCragName());
    }

    @Transactional
    public void deleteRoute(Long id) {
        routeRepository.deleteById(id);
    }

    private Crag createCrag(String cragName) {
        Crag crag = new Crag();
        crag.setName(cragName);
        return crag;
    }

    private Wall createWall(String wallName, Crag crag) {
        Wall wall = new Wall();
        wall.setName(wallName);
        wall.setCrag(crag);
        return wall;
    }
}
