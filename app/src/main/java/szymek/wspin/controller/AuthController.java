package szymek.wspin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import szymek.wspin.adapters.UserAdapter;
import szymek.wspin.dto.JwtRequest;
import szymek.wspin.dto.JwtResponse;
import szymek.wspin.util.JwtUtil;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
public class AuthController {
    private static final int EXPIRY_24H_IN_SECONDS = 60 * 60 * 24;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserAdapter userService;
    @Autowired
    JwtUtil jwtUtil;

    @PostMapping("/authenticateRest")
    JwtResponse authenticateRest(@Valid @RequestBody JwtRequest jwtRequest) throws Exception {
        authenticate(jwtRequest);
        UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getName());
        String token = jwtUtil.generateToken(userDetails);
        return new JwtResponse(token);
    }

    @PostMapping("/authenticateBrowser")
    ResponseEntity<?> authenticateBrowser(@Valid @RequestBody JwtRequest jwtRequest, HttpServletResponse response)
            throws Exception {
        authenticate(jwtRequest);
        response.addCookie(createCookie(jwtRequest));
        return ResponseEntity.ok("access_token set with httpOnly parameter");
    }

    private Cookie createCookie(JwtRequest jwtRequest) {
        UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getName());
        String token = jwtUtil.generateToken(userDetails);
        Cookie accessTokenCookie = new Cookie("access_token", token);
        accessTokenCookie.setHttpOnly(true);
        accessTokenCookie.setMaxAge(EXPIRY_24H_IN_SECONDS);
        return accessTokenCookie;
    }

    private void authenticate(JwtRequest jwtRequest) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    jwtRequest.getName(), jwtRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
