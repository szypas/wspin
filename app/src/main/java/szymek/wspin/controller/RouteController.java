package szymek.wspin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import szymek.wspin.adapters.RouteAdapter;
import szymek.wspin.dto.RouteDto;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
public class RouteController {
    @Autowired
    RouteAdapter routeService;

    @GetMapping("/routes")
    List<RouteDto> getRoutes() {
        return routeService.getAllRoutes();
    }

    @PostMapping("/routes")
    ResponseEntity<?> addRoute(@Valid @RequestBody RouteDto routeDto) {
        if (routeService.getRoute(routeDto) != null)
            return ResponseEntity.unprocessableEntity().build();
        RouteDto route = routeService.addRoute(routeDto);
        return ResponseEntity.ok(route);
    }

    @DeleteMapping("/routes/{id}")
    ResponseEntity<?> deleteRoute(@PathVariable String id) {
        try {
            routeService.deleteRoute(id);
        } catch (DataIntegrityViolationException exception) {
            return ResponseEntity.badRequest().body("Route cannot be deleted - probably used in some ascents");
        }
        return ResponseEntity.noContent().build();
    }
}
