package szymek.wspin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import szymek.wspin.adapters.UserAdapter;
import szymek.wspin.dto.UserDto;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
public class RegisterController {
    @Autowired
    UserAdapter userService;

    @PostMapping("/users")
    public ResponseEntity<?> register(@Valid @RequestBody UserDto userDto) {
        if (userService.getByName(userDto.getLogin()) == null) {
            UserDto user = userService.save(userDto);
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    @DeleteMapping("/users")
    ResponseEntity<?> deleteAscent(Principal principal) {
        userService.deleteUser(principal.getName());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/users-registerAdmin")
    public ResponseEntity<?> registerAdmin(@RequestBody UserDto userDto, Principal principal) {
        if (userService.getByName(userDto.getLogin()) == null) {
            List<String> roles = Arrays.asList("ROLE_USER", "ROLE_ADMIN");
            UserDto user = userService.save(userDto, roles);
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.unprocessableEntity().build();
        }
    }
}
