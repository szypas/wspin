package szymek.wspin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import szymek.wspin.adapters.AscentAdapter;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.util.AscentSearchFilter;

import javax.persistence.EntityExistsException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
public class AscentController {
    @Autowired
    AscentAdapter ascentService;

    @GetMapping("/ascents")
    List<AscentDto> getAscents(Principal principal, AscentSearchFilter filter) {
        log.debug(filter.toString());
        return ascentService.getAscents(principal.getName(), filter);
    }

    @PostMapping("/ascents")
    ResponseEntity<?> addAscent(@Valid @RequestBody AscentDto ascentDto, Principal principal) {
        log.debug("adding ascent: " + ascentDto);
        try {
            return ResponseEntity.ok(ascentService.addAscent(ascentDto, principal.getName()));
        } catch (EntityExistsException ignore) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @DeleteMapping("/ascents/{id}")
    ResponseEntity<?> deleteAscent(@PathVariable String id, Principal principal) {
        List<AscentDto> ascent = ascentService.getAscents(principal.getName(), new AscentSearchFilter());
        boolean userHasAscentWithGivenId = ascent.stream().anyMatch(ascentDto -> Objects.equals(ascentDto.getId(), id));
        if (userHasAscentWithGivenId) {
            ascentService.removeAscent(id, principal.getName());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        // todo move to service
    }


}
