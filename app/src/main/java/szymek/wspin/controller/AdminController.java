package szymek.wspin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import szymek.wspin.adapters.UserAdapter;
import szymek.wspin.dto.UserDto;

import java.util.List;

@RestController
public class AdminController {
    @Autowired
    UserAdapter userService;

    @GetMapping("/users")
    public List<UserDto> getUsers() {
        return userService.getAllUsers();
    }
}
