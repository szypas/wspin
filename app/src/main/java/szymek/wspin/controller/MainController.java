package szymek.wspin.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class MainController {
    @ResponseBody
    @GetMapping("/")
    public ResponseEntity<?> showLandingPage(HttpServletRequest request, Principal principal) {
        String userName = principal == null ? "anonymous" : principal.getName();
        return ResponseEntity.ok("Hello " + userName + "! Test API: " + request.getRequestURL().toString() + "swagger-ui.html");
    }

    @GetMapping("/js")
    public String showJsPage() {
        return "index";
    }
}
