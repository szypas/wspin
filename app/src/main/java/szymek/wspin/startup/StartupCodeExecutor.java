package szymek.wspin.startup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StartupCodeExecutor {
    @Value("${cors_allowed_origin:not_specified}")
    String allowedOrigin;
    @Autowired
    private StartupCode startupCode;

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
//        startupCode.execute();
        log.info("CORS Allowed origin: " + allowedOrigin);
    }
}
