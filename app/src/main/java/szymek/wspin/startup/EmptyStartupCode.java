package szymek.wspin.startup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmptyStartupCode implements StartupCode{
    @Override
    public void execute() {
        log.info("Empty startup code executed");
    }
}
