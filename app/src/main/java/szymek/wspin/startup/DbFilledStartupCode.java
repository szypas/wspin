package szymek.wspin.startup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.service.HibernateAscentService;
import szymek.wspin.hibernate.service.HibernateRouteService;
import szymek.wspin.hibernate.service.HibernateUserService;
import szymek.wspin.util.Status;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Primary
@Profile({"rds", "h2"})
@Component
@Slf4j
public class DbFilledStartupCode implements StartupCode {
    @Autowired
    HibernateUserService userService;
    @Autowired
    HibernateRouteService routeService;
    @Autowired
    HibernateAscentService ascentService;

    @Override
    public void execute() {
        log.trace("Filling database with dummy data:");
        fillWithDummyData();
        log.info("Database filled succesfully");
        log.info("H2 startup code executed");
    }

    private void fillWithDummyData() {
        UserDto userDto1 = new UserDto("user1", "user1@email", "123");
        UserDto userDto2 = new UserDto("user2", "user2@email", "123");
        UserDto adminDto = new UserDto("admin", "admin@email", "123");
        List<Role> adminRoles = Arrays.asList(new Role("ROLE_USER"), new Role("ROLE_ADMIN"));
        log.trace(userDto1.toString());
        log.trace(userDto2.toString());
        log.trace(adminDto.toString());
        log.trace("admin with roles: " + adminRoles);
        userService.save(userDto1);
        userService.save(userDto2);
        userService.save(adminDto, adminRoles);
        RouteDto routeDto1 = new RouteDto("obladi-oblada", "labajowa sciana", "grupa labajowej");
        RouteDto routeDto2 = new RouteDto("sekret mnicha", "mniszkowa", "grupa labajowej");
        RouteDto routeDto3 = new RouteDto("inny swiat", "cyganska turnia", "brzoskwinia");
        RouteDto routeDto4 = new RouteDto("pojedynek z wiatrem", "przelaz", "sloneczne skaly");
        RouteDto routeDto5 = new RouteDto("spitostrada", "ostatnia", "brzoskwinia");
        RouteDto routeDto6 = new RouteDto("hipokryzja", "cyganska turnia", "brzoskwinia");
        RouteDto routeDto7 = new RouteDto("hiszpanskie czary", "zytnie skaly", "zytnie skaly");
        RouteDto routeDto8 = new RouteDto("francuskie stosy", "zytnie skaly", "zytnie skaly");
        RouteDto routeDto9 = new RouteDto("sadystowka", "sadystowka", "zakrzowek");
        RouteDto routeDto10 = new RouteDto("prawy meningitis", "murarze", "dupa slonia");
        RouteDto routeDto11 = new RouteDto("filar lesnego dziadka", "lyse plecy", "lyse skaly");
        RouteDto routeDto12 = new RouteDto("beatyfikacja", "kazalnica", "brzoskwinia");
        log.trace(routeDto1.toString());
        log.trace(routeDto2.toString());
        log.trace(routeDto3.toString());
        log.trace(routeDto4.toString());
        log.trace(routeDto5.toString());
        log.trace(routeDto6.toString());
        log.trace(routeDto7.toString());
        log.trace(routeDto8.toString());
        log.trace(routeDto9.toString());
        log.trace(routeDto10.toString());
        log.trace(routeDto11.toString());
        log.trace(routeDto12.toString());
        routeService.addRoute(routeDto1);
        routeService.addRoute(routeDto2);
        routeService.addRoute(routeDto3);
        routeService.addRoute(routeDto4);
        routeService.addRoute(routeDto5);
        routeService.addRoute(routeDto6);
        routeService.addRoute(routeDto7);
        routeService.addRoute(routeDto8);
        routeService.addRoute(routeDto9);
        routeService.addRoute(routeDto10);
        routeService.addRoute(routeDto11);
        routeService.addRoute(routeDto12);
        AscentDto ascent1 = new AscentDto(routeDto1, "VI.2+", Status.FL, createUtilDate(2017, 7, 15));
        AscentDto ascent2 = new AscentDto(routeDto2, "V", Status.FL, createUtilDate(2018, 7, 15));
        AscentDto ascent3 = new AscentDto(routeDto3, "VI.2+", Status.Todo, null);
        AscentDto ascent4 = new AscentDto(routeDto4, "VI.4", Status.Todo_Tried, null);
        AscentDto ascent5 = new AscentDto(routeDto5, "VI.2+", Status.OS, createUtilDate(2016, 7, 15));
        AscentDto ascent6 = new AscentDto(routeDto6, "VI.4", Status.RP, createUtilDate(2015, 7, 15));
        AscentDto ascent7 = new AscentDto(routeDto7, "VI.3+", Status.RP, createUtilDate(2017, 11, 11));
        AscentDto ascent8 = new AscentDto(routeDto8, "VI.2", Status.OS, createUtilDate(2018, 1, 1));
        AscentDto ascent9 = new AscentDto(routeDto9, "VI.3", Status.Project, null);
        AscentDto ascent10 = new AscentDto(routeDto10, "VI.2+", Status.Todo_Tried, null);
        AscentDto ascent11 = new AscentDto(routeDto11, "VI.1+", Status.OS, createUtilDate(2019, 7, 15));
        AscentDto ascent12 = new AscentDto(routeDto12, "VI.1+", Status.RP, createUtilDate(2020, 7, 15));
        log.trace(ascent1.toString());
        log.trace(ascent2.toString());
        log.trace(ascent3.toString());
        log.trace(ascent4.toString());
        log.trace(ascent5.toString());
        log.trace(ascent6.toString());
        log.trace(ascent7.toString());
        log.trace(ascent8.toString());
        log.trace(ascent9.toString());
        log.trace(ascent10.toString());
        log.trace(ascent11.toString());
        log.trace(ascent12.toString());
        User user1 = userService.getByName(userDto1.getLogin());
        ascentService.addAscent(ascent1, user1.getLogin());
        ascentService.addAscent(ascent2, user1.getLogin());
        ascentService.addAscent(ascent3, user1.getLogin());
        ascentService.addAscent(ascent4, user1.getLogin());
        ascentService.addAscent(ascent5, user1.getLogin());
        ascentService.addAscent(ascent6, user1.getLogin());
        ascentService.addAscent(ascent7, user1.getLogin());
        ascentService.addAscent(ascent8, user1.getLogin());
        ascentService.addAscent(ascent9, user1.getLogin());
        ascentService.addAscent(ascent10, user1.getLogin());
        ascentService.addAscent(ascent11, user1.getLogin());
        ascentService.addAscent(ascent12, user1.getLogin());
    }

    private Date createUtilDate(int year, int month, int dayOfMonth) {
        return Date.from(LocalDate.of(year, month, dayOfMonth).atStartOfDay().toInstant(ZoneOffset.UTC));
    }
}
