package szymek.wspin.startup;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dynamodb.repository.RouteRepository;
import szymek.wspin.dynamodb.repository.UserRepository;
import szymek.wspin.dynamodb.repository.ascentRepository.AscentRepository;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

@Primary
@Component
@Profile("ddb")
@AllArgsConstructor
public class DynamoDbStartupCode implements StartupCode {

    private final RouteRepository routeRepository;
    private final UserRepository userRepository;
    private final AscentRepository ascentRepository;

    @Override
    public void execute() {

        LocalDate localDate = LocalDate.of(2021,11,11);
        Instant instant = localDate.atStartOfDay().toInstant(ZoneOffset.UTC);
        Date x = new Date(instant.toEpochMilli());

        routeRepository.deleteAll();
        userRepository.deleteAll();
        ascentRepository.deleteAll();
        System.out.println(" === Dynamodb cleaned === ");
    }
}
