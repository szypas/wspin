package szymek.wspin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import szymek.wspin.config.HibernateConfig;

@SpringBootApplication
@Import({HibernateConfig.class})
public class WspinApplication {
    public static void main(String[] args) {
        SpringApplication.run(WspinApplication.class, args);
    }
}
