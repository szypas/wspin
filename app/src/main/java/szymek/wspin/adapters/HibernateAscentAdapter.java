package szymek.wspin.adapters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Ascent;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.service.HibernateAscentService;
import szymek.wspin.util.AscentSearchFilter;

import java.util.List;
import java.util.stream.Collectors;

@Profile({"h2", "rds"})
@Component
public class HibernateAscentAdapter implements AscentAdapter {
    private final HibernateAscentService ascentService;

    @Autowired
    public HibernateAscentAdapter(HibernateAscentService ascentService) {
        this.ascentService = ascentService;
    }

    @Override
    public AscentDto addAscent(AscentDto ascentDto, String userName) {
        return createAscentDto(ascentService.addAscent(ascentDto, userName));
    }

    @Override
    public List<AscentDto> getAscents(String userName, AscentSearchFilter filter) {
        return ascentService.getAscents(userName, filter).stream().map(this::createAscentDto).collect(Collectors.toList());
    }

    @Override
    public void removeAscent(String id, String userName) {
        ascentService.removeAscent(Long.parseLong(id), userName);
    }

    private AscentDto createAscentDto(Ascent ascent) {
        if (ascent == null) {
            return null;
        }
        return new AscentDto(createRouteDto(ascent.getRoute()), ascent.getDifficulty(), ascent.getStatus(), ascent.getDate(), ascent.getId().toString());
    }

    private RouteDto createRouteDto(Route route) {
        Wall wall = route.getWall();
        Crag crag = wall.getCrag();
        return new RouteDto(route.getName(), wall.getName(), crag.getName(), route.getId().toString());
    }
}
