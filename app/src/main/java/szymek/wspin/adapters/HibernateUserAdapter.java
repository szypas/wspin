package szymek.wspin.adapters;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;
import szymek.wspin.hibernate.model.User;
import szymek.wspin.hibernate.service.HibernateUserService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Profile({"h2", "rds"})
@Component
@AllArgsConstructor
public class HibernateUserAdapter implements UserAdapter {
    private final HibernateUserService userService;

    @Override
    public UserDto save(UserDto userDto) {
        return createUserDto(userService.save(userDto));
    }

    @Override
    public UserDto save(UserDto userDto, List<String> roles) {
        return createUserDto(userService.save(userDto, roles.stream().map(Role::new).collect(Collectors.toList())));
    }

    @Override
    public UserDto getByName(String name) {
        return createUserDto(userService.getByName(name));
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers().stream().map(this::createUserDto).collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userService.loadUserByUsername(s);
    }

    @Override
    public void deleteUser(String name) {
        userService.deleteByLogin(name);
    }

    private UserDto createUserDto(User user) {
        if (user == null) {
            return null;
        }
        return new UserDto(user.getLogin(), user.getEmail(), user.getPassword(), getRolesAsStrings(user.getRoles()));
    }

    private List<String> getRolesAsStrings(Collection<Role> roles) {
        if (roles == null) {
            return null;
        }
        return roles.stream().map(Role::getName).collect(Collectors.toList());
    }
}
