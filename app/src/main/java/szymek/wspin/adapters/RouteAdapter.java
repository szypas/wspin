package szymek.wspin.adapters;

import szymek.wspin.dto.RouteDto;

import java.util.List;

public interface RouteAdapter {
    List<RouteDto> getAllRoutes();

    RouteDto addRoute(RouteDto routeDto);

    RouteDto getRoute(RouteDto routeDto);

    void deleteRoute(String id);
}
