package szymek.wspin.adapters;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dynamodb.model.Ascent;
import szymek.wspin.dynamodb.service.DynamoAscentService;
import szymek.wspin.util.AscentSearchFilter;

import java.util.List;
import java.util.stream.Collectors;

@Profile("ddb")
@Component
@AllArgsConstructor
public class DynamoAscentAdapter implements AscentAdapter {

    private final DynamoAscentService ascentService;

    @Override
    public AscentDto addAscent(AscentDto ascentDto, String userName) {
        return createAscentDto(ascentService.addAscent(ascentDto, userName));
    }

    private AscentDto createAscentDto(Ascent ascent) {
        if (ascent == null) return null;
        RouteDto routeDto = null;
        if (ascent.getRoute() != null) {
            routeDto = new RouteDto(ascent.getRoute().getRouteName(), ascent.getRoute().getWallName(),
                    ascent.getRoute().getCragName(), ascent.getRoute().getId());
        }
        return new AscentDto(routeDto, ascent.getDifficulty(), ascent.getAscentStatus(), ascent.getAscentDate(), ascent.getId());
    }

    @Override
    public List<AscentDto> getAscents(String userName, AscentSearchFilter filter) {
//       return ascentService.getAscents(userName).stream().map(this::createAscentDto).collect(Collectors.toList());
       return ascentService.getAscents(userName, filter).stream().map(this::createAscentDto).collect(Collectors.toList());
    }

    @Override
    public void removeAscent(String id, String userName) {
        ascentService.removeAscent(id);
    }
}
