package szymek.wspin.adapters;

import org.springframework.security.core.userdetails.UserDetails;
import szymek.wspin.dto.UserDto;

import java.util.List;

public interface UserAdapter {
    UserDto save(UserDto userDto);

    UserDto save(UserDto userDto, List<String> roles);

    UserDto getByName(String name);

    List<UserDto> getAllUsers();

    UserDetails loadUserByUsername(String s);

    void deleteUser(String name);
}
