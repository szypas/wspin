package szymek.wspin.adapters;

import szymek.wspin.dto.AscentDto;
import szymek.wspin.util.AscentSearchFilter;

import java.util.List;

public interface AscentAdapter {
    AscentDto addAscent(AscentDto ascentDto, String userName);

    List<AscentDto> getAscents(String userName, AscentSearchFilter filter);

    void removeAscent(String id, String userName);
}
