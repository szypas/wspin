package szymek.wspin.adapters;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.UserDto;
import szymek.wspin.dynamodb.model.User;
import szymek.wspin.dynamodb.service.DynamoUserService;

import java.util.List;
import java.util.stream.Collectors;

@Profile("ddb")
@Component
@AllArgsConstructor
public class DynamoUserAdapter implements UserAdapter {
    private final DynamoUserService userService;

    @Override
    public UserDto save(UserDto userDto) {
        return createUserDto(userService.save(userDto));
    }

    @Override
    public UserDto save(UserDto userDto, List<String> roles) {
        return createUserDto(userService.save(userDto, roles));
    }

    @Override
    public UserDto getByName(String name) {
        return createUserDto(userService.getByName(name));
    }

    @Override
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers().stream().map(this::createUserDto).collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userService.loadUserByUsername(username);
    }

    @Override
    public void deleteUser(String name) {
        userService.deleteByLogin(name);
    }

    private UserDto createUserDto(User user) {
        if (user == null) {
            return null;
        }
        return new UserDto(user.getLogin(), user.getEmail(), user.getPassword(), user.getRoles());
    }
}
