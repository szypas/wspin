package szymek.wspin.adapters;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dynamodb.model.Route;
import szymek.wspin.dynamodb.service.DynamoRouteService;

import java.util.List;
import java.util.stream.Collectors;

@Profile("ddb")
@Component
@AllArgsConstructor
public class DynamoRouteAdapter implements RouteAdapter {
    private final DynamoRouteService routeService;

    @Override
    public List<RouteDto> getAllRoutes() {
        return routeService.getAllRoutes().stream().map(this::createRouteDto).collect(Collectors.toList());
    }

    @Override
    public RouteDto addRoute(RouteDto routeDto) {
        return createRouteDto(routeService.addRoute(routeDto));
    }

    @Override
    public RouteDto getRoute(RouteDto routeDto) {
        return createRouteDto(routeService.getRoute(routeDto));
    }

    private RouteDto createRouteDto(Route route) {
        if (route == null) {
            return null;
        }
        return new RouteDto(route.getRouteName(), route.getWallName(), route.getCragName(), route.getId());
    }

    @Override
    public void deleteRoute(String id) {
        routeService.deleteRoute(id);
    }
}
