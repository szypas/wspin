package szymek.wspin.adapters;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;
import szymek.wspin.hibernate.service.HibernateRouteService;

import java.util.List;
import java.util.stream.Collectors;

@Profile({"h2", "rds"})
@Component
public class HibernateRouteAdapter implements RouteAdapter {
    private final HibernateRouteService routeService;

    public HibernateRouteAdapter(HibernateRouteService routeService) {
        this.routeService = routeService;
    }

    @Override
    public List<RouteDto> getAllRoutes() {
        return routeService.getAllRoutes().stream().map(this::createRouteDto).collect(Collectors.toList());
    }

    @Override
    public RouteDto addRoute(RouteDto routeDto) {
        return createRouteDto(routeService.addRoute(routeDto));
    }

    @Override
    public RouteDto getRoute(RouteDto routeDto) {
        return createRouteDto(routeService.getRoute(routeDto));
    }

    @Override
    public void deleteRoute(String id) {
        routeService.deleteRoute(Long.parseLong(id));
    }

    private RouteDto createRouteDto(Route route) {
        if (route == null) {
            return null;
        }
        Wall wall = route.getWall();
        Crag crag = wall.getCrag();
        return new RouteDto(route.getName(), wall.getName(), crag.getName(), route.getId().toString());
    }
}
