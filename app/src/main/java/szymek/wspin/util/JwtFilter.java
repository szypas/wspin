package szymek.wspin.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import szymek.wspin.adapters.UserAdapter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserAdapter userService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        log.debug("handling " + httpServletRequest);
        String token = getAccessToken(httpServletRequest);
        if (isValid(token) && jwtUtil.isNotExpired(token)) {
            String username = jwtUtil.getUsername(token);
            if (username != null && userNotAuthenticated()) {
                UserDetails userDetails = userService.loadUserByUsername(username);
                WebAuthenticationDetails authenticationDetails = new WebAuthenticationDetailsSource().buildDetails(httpServletRequest);
                authenticateUser(userDetails, authenticationDetails);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private boolean isValid(String token) {
        return token != null && !token.equals("null");
    }

    private void authenticateUser(UserDetails userDetails, WebAuthenticationDetails authenticationDetails) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        usernamePasswordAuthenticationToken.setDetails(authenticationDetails);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    private boolean userNotAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }

    private String getAccessToken(HttpServletRequest httpServletRequest) {
        String token = null;
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            token = getTokenFromCookies(cookies);
        } else {
            log.debug("cookies are empty");
        }
        if (token == null) {
            token = getTokenFromAuthorizationHeader(httpServletRequest);
        }
        return token;
    }

    private String getTokenFromAuthorizationHeader(HttpServletRequest httpServletRequest) {
        String authorization = httpServletRequest.getHeader("Authorization");
        if (authorization != null && authorization.contains("Bearer ")) {
            log.debug("Token found in Authorization Bearer: " + authorization.substring(7));
            return authorization.substring(7);
        }
        log.debug("Token not found in Authorization Bearer.");
        log.trace("auth type: "+ httpServletRequest.getAuthType());
        return null;
    }

    private String getTokenFromCookies(Cookie[] cookies) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("access_token")) {
                log.debug("Token found in cookie: " + cookie.getValue());
                return cookie.getValue();
            }
        }
        log.debug("Token not found in cookies.");
        return null;
    }
}
