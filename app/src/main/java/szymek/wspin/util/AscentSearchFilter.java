package szymek.wspin.util;

import lombok.Data;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
public class AscentSearchFilter {
    private String difficultyFrom = "I";
    private String difficultyTo = "VI.9+";
    private Set<Status> statuses = new HashSet<>(Arrays.asList(
            Status.Project, Status.Tried, Status.Todo, Status.Todo_Tried, Status.OS, Status.RP, Status.FL));
    private Date dateFrom;
    private Date dateTo;

    public void setDifficultyFrom(String difficultyFrom) {
        this.difficultyFrom = Objects.requireNonNullElse(difficultyFrom, "I");
    }

    public void setDifficultyTo(String difficultyTo) {
        this.difficultyTo = Objects.requireNonNullElse(difficultyTo, "VI.9+");
    }

    public void setStatuses(Set<Status> statuses) {
        this.statuses = Objects.requireNonNullElseGet(statuses, () -> Set.of(
                Status.Project, Status.Tried, Status.Todo, Status.Todo_Tried, Status.OS, Status.RP, Status.FL));
    }

}
