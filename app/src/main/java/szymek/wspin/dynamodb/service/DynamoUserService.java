package szymek.wspin.dynamodb.service;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import szymek.wspin.dto.UserDto;
import szymek.wspin.dynamodb.model.User;
import szymek.wspin.dynamodb.repository.UserRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Profile("ddb")
@Service
@AllArgsConstructor
public class DynamoUserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public User save(UserDto userDto) {
        User newUser = new User(
                userDto.getLogin(),
                userDto.getEmail(),
                passwordEncoder.encode(userDto.getPassword()),
                List.of("ROLE_USER"));
        userRepository.save(newUser);
        return newUser;
    }

    public User save(UserDto userDto, List<String> roles) {
        User newUser = new User(
                userDto.getLogin(),
                userDto.getEmail(),
                passwordEncoder.encode(userDto.getPassword()),
                roles);
        userRepository.save(newUser);
        return newUser;
    }

    public User getByName(String name) {
        return userRepository.findByLogin(name);
    }

    public List<User> getAllUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(List<String> roles) {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public void deleteByLogin(String name) {
        userRepository.deleteByLogin(name);
    }
}
