package szymek.wspin.dynamodb.service;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dynamodb.model.Ascent;
import szymek.wspin.dynamodb.model.Route;
import szymek.wspin.dynamodb.model.User;
import szymek.wspin.dynamodb.repository.ascentRepository.AscentRepository;
import szymek.wspin.util.AscentSearchFilter;

import javax.persistence.EntityExistsException;
import java.sql.Date;
import java.util.List;

@Profile("ddb")
@Service
@AllArgsConstructor
public class DynamoAscentService {
    private final AscentRepository ascentRepository;
    private final DynamoRouteService routeService;
    private final DynamoUserService userService;

    public Ascent addAscent(AscentDto ascentDto, String userName) {
        User user = userService.getByName(userName);
        Route route = routeService.getRoute(ascentDto.getRouteDto());
        if (route != null) {
            Ascent ascent = ascentRepository.findByRouteAndUser(route, user);
            if (ascent != null) {
                throw new EntityExistsException();
            }
        } else {
            route = routeService.addRoute(ascentDto.getRouteDto());
        }
        return addAscent(ascentDto, route, user);

    }

    private Ascent addAscent(AscentDto ascentDto, Route route, User user) {
        Ascent ascent = new Ascent();
        ascent.setRoute(route);
        ascent.setDifficulty(ascentDto.getDifficulty());
        ascent.setAscentStatus(ascentDto.getStatus());
        ascent.setUser(user);
        ascent.setAscentDate(ascentDto.getDate() == null ? null : new Date(ascentDto.getDate().toInstant().toEpochMilli()));
        ascentRepository.save(ascent);
        return ascent;
    }

    public List<Ascent> getAscents(String userName, AscentSearchFilter filter) {
        User user = userService.getByName(userName);
        return ascentRepository.findByUser(user, filter);
    }

    public void removeAscent(String id) {
        ascentRepository.deleteById(id);
    }
}
