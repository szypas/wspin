package szymek.wspin.dynamodb.service;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dynamodb.model.Route;
import szymek.wspin.dynamodb.repository.RouteRepository;

import java.util.List;

@Profile("ddb")
@AllArgsConstructor
@Service
public class DynamoRouteService {
    private final RouteRepository routeRepository;

    public List<Route> getAllRoutes() {
        return Lists.newArrayList(routeRepository.findAll());
    }

    public Route addRoute(RouteDto routeDto) {
        Route route = getRoute(routeDto);
        if (route != null) {
            return route;
        }
        return routeRepository.save(new Route(routeDto.getRouteName(), routeDto.getWallName(), routeDto.getCragName()));
    }

    public Route getRoute(RouteDto routeDto) {
        return routeRepository.findByRouteNameAndWallNameAndCragName(
                routeDto.getRouteName(),
                routeDto.getWallName(),
                routeDto.getCragName());
    }

    public void deleteRoute(String id) {
        routeRepository.deleteById(id);
    }
}
