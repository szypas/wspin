package szymek.wspin.dynamodb.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import szymek.wspin.dynamodb.model.User;

@EnableScan
public interface UserRepository extends CrudRepository<User, String> {
    User findByLogin(String s);
    void deleteByLogin(String s);
}
