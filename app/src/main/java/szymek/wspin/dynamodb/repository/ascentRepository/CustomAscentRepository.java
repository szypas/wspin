package szymek.wspin.dynamodb.repository.ascentRepository;

import szymek.wspin.dynamodb.model.Ascent;
import szymek.wspin.dynamodb.model.User;
import szymek.wspin.util.AscentSearchFilter;

import java.util.List;

public interface CustomAscentRepository {
    List<Ascent> findByUser(User user, AscentSearchFilter filter);
}
