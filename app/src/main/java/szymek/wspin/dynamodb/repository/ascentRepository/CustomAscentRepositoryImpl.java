package szymek.wspin.dynamodb.repository.ascentRepository;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import lombok.extern.slf4j.Slf4j;
import szymek.wspin.dynamodb.model.Ascent;
import szymek.wspin.dynamodb.model.User;
import szymek.wspin.util.AscentSearchFilter;
import szymek.wspin.util.Status;
import szymek.wspin.util.difficulty.DifficultyUtil;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Slf4j
public class CustomAscentRepositoryImpl implements CustomAscentRepository {

    private static final DynamoDBMapper DYNAMO_DB_MAPPER = new DynamoDBMapper(AmazonDynamoDBClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build());
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private static final String DIFFICULTY_VALUE_PLACEHOLDER = ":di";
    private static final String STATUS_VALUE_PLACEHOLDER = ":st";
    private static final String STATUS_KEY_PLACEHOLDER = "#st";

    public void CustomAscentRepository() {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("CET"));
    }

    @Override
    public List<Ascent> findByUser(User user, AscentSearchFilter filter) {
        String dateFrom = filter.getDateFrom() == null ? null : DATE_FORMATTER.format(filter.getDateFrom());
        String dateTo = filter.getDateTo() == null ? null : DATE_FORMATTER.format(filter.getDateTo());

        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":userName", new AttributeValue().withS(user.getLogin()));
        eav.putAll(createStatusExpressionAttributeValues(filter.getStatuses().stream().toList()));
        eav.putAll(createDateExpressionAttributeValues(dateFrom, dateTo));
        eav.putAll(createDifficultyExpressionAttributeValues(filter.getDifficultyFrom(), filter.getDifficultyTo()));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("#us.login = :userName " + getFilterExpression(filter, dateFrom, dateTo))
                .withExpressionAttributeValues(eav)
                .withExpressionAttributeNames(Map.of("#us", "user", STATUS_KEY_PLACEHOLDER, "ascentStatus"));

        return DYNAMO_DB_MAPPER.scan(Ascent.class, scanExpression);
    }

    private String getFilterExpression(AscentSearchFilter filter, String dateFrom, String dateTo) {
        String dateToExpression = dateTo == null ? "" : " and ascentDate <= :dat ";
        String dateFromExpression = dateFrom == null ? "" : " and ascentDate >= :daf ";
        String statusExpression = createStatusExpression(filter.getStatuses().stream().toList());
        String difficultyExpression = createDifficultyExpression(filter.getDifficultyFrom(), filter.getDifficultyTo());
        return dateToExpression + dateFromExpression + statusExpression + difficultyExpression;
    }

    private Map<String, AttributeValue> createDateExpressionAttributeValues(String dateFrom, String dateTo) {
        Map<String, AttributeValue> eav = new HashMap<>();
        if (dateFrom != null) {
            eav.put(":daf", new AttributeValue().withS(dateFrom));
        }
        if (dateTo != null) {
            eav.put(":dat", new AttributeValue().withS(dateTo));
        }
        return eav;
    }

    private Map<String, AttributeValue> createStatusExpressionAttributeValues(List<Status> statuses) {
        if (statuses.isEmpty()) {
            return Map.of();
        }
        Map<String, AttributeValue> mapa = new HashMap<>();
        for(int i=0; i<statuses.size(); i++) {
            mapa.put(STATUS_VALUE_PLACEHOLDER + i, new AttributeValue(statuses.get(i).toString()));
        }
        return mapa;
    }

    private Map<String, AttributeValue> createDifficultyExpressionAttributeValues(String dif, String dit) {
        List<String> diffuculties = DifficultyUtil.generateListOfDifficulties(dif, dit);
        if (diffuculties.isEmpty()) {
            return Map.of();
        }
        Map<String, AttributeValue> mapa = new HashMap<>();
        for(int i=0; i<diffuculties.size(); i++) {
            mapa.put(DIFFICULTY_VALUE_PLACEHOLDER + i, new AttributeValue(diffuculties.get(i)));
        }
        return mapa;
    }

    private String createStatusExpression(List<Status> statuses) {
        if (statuses.isEmpty()) {
            return "";
        }
        StringBuilder expression = new StringBuilder();
        expression.append(" and (");
        int size = statuses.size();
        for(int i=0; i < size - 1; i++) {
            expression.append(STATUS_KEY_PLACEHOLDER).append(" = ").append(STATUS_VALUE_PLACEHOLDER).append(i).append(" or ");
        }
        expression.append(STATUS_KEY_PLACEHOLDER).append(" = ").append(STATUS_VALUE_PLACEHOLDER).append(size - 1);
        expression.append(") ");
        return expression.toString();
    }

    private String createDifficultyExpression(String dif, String dit) {
        List<String> diffuculties = DifficultyUtil.generateListOfDifficulties(dif, dit);
        if (diffuculties.isEmpty()) {
            return "";
        }
        StringBuilder expression = new StringBuilder();
        expression.append(" and (");
        int size = diffuculties.size();
        for(int i=0; i < size - 1; i++) {
            expression.append("difficulty = ").append(DIFFICULTY_VALUE_PLACEHOLDER).append(i).append(" or ");
        }
        expression.append("difficulty= ").append(DIFFICULTY_VALUE_PLACEHOLDER).append(size - 1);
        expression.append(") ");
        return expression.toString();
    }
}

