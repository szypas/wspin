package szymek.wspin.dynamodb.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import szymek.wspin.dynamodb.model.Route;

@EnableScan
public interface RouteRepository extends CrudRepository<Route, String> {
    Route findByRouteNameAndWallNameAndCragName(String routeName, String wallName, String cragName);
}
