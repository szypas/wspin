package szymek.wspin.dynamodb.repository.ascentRepository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import szymek.wspin.dynamodb.model.Ascent;
import szymek.wspin.dynamodb.model.Route;
import szymek.wspin.dynamodb.model.User;

import java.util.List;

@EnableScan
public interface AscentRepository extends CrudRepository<Ascent, String>, CustomAscentRepository {
    Ascent findByRouteAndUser(Route route, User user);
    List<Ascent> findByUser(User user);
}
