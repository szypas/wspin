package szymek.wspin.dynamodb.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import szymek.wspin.util.Status;

public class MyStatusConverter implements DynamoDBTypeConverter<String, Status> {

    @Override
    public String convert(Status status) {
        return status.toString();
    }

    @Override
    public Status unconvert(String string) {
        return Status.valueOf(string);
    }
}
