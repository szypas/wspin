package szymek.wspin.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import szymek.wspin.dynamodb.repository.RouteRepository;
import szymek.wspin.dynamodb.repository.UserRepository;
import szymek.wspin.dynamodb.repository.ascentRepository.AscentRepository;

@Profile("ddb")
@Configuration
@EnableJpaRepositories(
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        classes = {AscentRepository.class, RouteRepository.class, UserRepository.class})}
)
@EnableDynamoDBRepositories(
        basePackages = "szymek.wspin.dynamodb.repository",
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                        classes = { AscentRepository.class, RouteRepository.class, UserRepository.class})})
public class DynamoDbConfig {

    @Value("#{environment.AWS_ACCESS_KEY}")
    private String AWS_ACCESS_KEY;

    @Value("#{environment.AWS_SECRET_ACCESS_KEY}")
    private String AWS_SECRET_ACCESS_KEY;


    public AWSCredentialsProvider amazonAWSCredentialsProvider() {
        return new AWSStaticCredentialsProvider(amazonAWSCredentials());
    }

    @Bean
    public AWSCredentials amazonAWSCredentials() {
        return new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY);
    }

    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(amazonAWSCredentialsProvider())
                .withRegion(Regions.EU_CENTRAL_1)
                .build();
    }
}

