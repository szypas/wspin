package szymek.wspin.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import szymek.wspin.hibernate.repository.CragRepository;
import szymek.wspin.hibernate.repository.RouteRepository;
import szymek.wspin.hibernate.repository.UserRepository;
import szymek.wspin.hibernate.repository.WallRepository;
import szymek.wspin.hibernate.repository.ascentRepository.AscentRepository;

// https://stackoverflow.com/questions/45663025/spring-data-jpa-multiple-enablejparepositories
@Profile({"h2", "rds"})
@Configuration
@EnableJpaRepositories(
        basePackages = {"szymek.wspin.hibernate.repository"},
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                        CragRepository.class, AscentRepository.class, RouteRepository.class, UserRepository.class,
                        WallRepository.class
                })
        })
public class HibernateConfig {
}
