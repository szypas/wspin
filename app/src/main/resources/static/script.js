let USERNAME, PASSWORD;

function closeAllTabs() {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
}

function addCellToRow(tr, content) {
    let nameCell = tr.insertCell();
    nameCell.textContent = content;
    nameCell.classList.add("table-data")
}

function insertRouteRowToRoutesTable(routesTable, rowData) {
    let tr = routesTable.insertRow();
    tr.classList.add("table-row")
    addCellToRow(tr, rowData.routeName);
    addCellToRow(tr, rowData.wallName);
    addCellToRow(tr, rowData.cragName);
}

function insertStatusRowToStatusesTable(routesTable, rowData) {
    let tr = routesTable.insertRow();
    tr.classList.add("table-row")
    addCellToRow(tr, rowData.routeDto.routeName);
    addCellToRow(tr, rowData.routeDto.wallName);
    addCellToRow(tr, rowData.routeDto.cragName);
    addCellToRow(tr, rowData.difficulty);
    addCellToRow(tr, rowData.status);
}

function addHeaderToRow(tr, content) {
    let titleNameCell = document.createElement('th');
    titleNameCell.textContent = content
    titleNameCell.classList.add("header__item")
    tr.append(titleNameCell)
}

function insertHeaderToRoutesTable(routesTable) {
    let tr = routesTable.insertRow();
    tr.classList.add("table-header")
    addHeaderToRow(tr, "Route Name");
    addHeaderToRow(tr, "Wall Name");
    addHeaderToRow(tr, "Crag Name");
}

function insertHeaderToStatusesTable(table) {
    let tr = table.insertRow();
    tr.classList.add("table-header")
    addHeaderToRow(tr, "Route Name");
    addHeaderToRow(tr, "Wall Name");
    addHeaderToRow(tr, "Crag Name");
    addHeaderToRow(tr, "Difficulty");
    addHeaderToRow(tr, "Status");

}

function createNewTable(id) {
    let newTable = document.createElement('table')
    newTable.classList.add("table")
    newTable.id = id
    return newTable
}

function fillTableOfRoutes(data){
    let oldTable = document.getElementById("routesTable");
    let table = createNewTable("routesTable")
    oldTable.replaceWith(table)
    insertHeaderToRoutesTable(table)
    for(let i=0; i < data.length; i++) {
        insertRouteRowToRoutesTable(table, data[i])
    }
}

function fillStatusesTable(data){
    let oldTable = document.getElementById("statusesTable");
    let table = createNewTable("statusesTable")
    oldTable.replaceWith(table)
    insertHeaderToStatusesTable(table)
    for(let i=0; i < data.length; i++) {
        insertStatusRowToStatusesTable(table, data[i])
    }
}

function getServerUrl() {
    console.log("location = " + location)
    if (location.href.includes("src/main/resources/templates/index.html")) return "http://localhost:8080"
    if (location.href.includes("http://localhost")) return "http://localhost:8080"
    if (location.href.includes("https://wspin.herokuapp.com")) return "https://wspin.herokuapp.com"
}

function refreshRoutesTable() {
    $.ajax({
        url: getServerUrl() + "/getRoutes",
        crossDomain: true,
        dataType: 'json'
    }).then(function (data) {
        fillTableOfRoutes(data)
    });
}

function refreshStatusesTable() {
    $.ajax({
        url: getServerUrl() + "/getAscents",
        crossDomain: true,
        dataType: 'json'
    }).done( function(data) {
        fillStatusesTable(data)
    }).fail(  function(request) {
        if (request.status === 401) alert("access denied. please log in.")
    });
}

function openRoutes() {
    closeAllTabs();
    document.getElementById("Routes").style.display = "block";
    document.getElementById("routesTabButton").className += " active";

    refreshRoutesTable()
}

function credentialsFilled() {
    return USERNAME !== undefined && PASSWORD !== undefined
}

function openStatuses() {
    if (!credentialsFilled()) {
        alert("please fill credentials");
        return;
    }
    closeAllTabs();
    document.getElementById("Statuses").style.display = "block";
    document.getElementById("statusesTabButton").className += " active";
    refreshStatusesTable()
}

function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

function authenticate() {

    $.ajax({
        type: "POST",
        url: "/authenticateBrowser",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify({ name: USERNAME, password: PASSWORD }),
        crossDomain: true,
    });

}



function saveCredentials() {
    USERNAME = document.getElementById("login_input").value
    PASSWORD = document.getElementById("password_input").value

    authenticate()

    closeForm()
}