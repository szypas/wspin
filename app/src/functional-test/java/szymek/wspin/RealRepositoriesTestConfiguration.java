package szymek.wspin;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import szymek.wspin.adapters.AscentAdapter;
import szymek.wspin.adapters.RouteAdapter;
import szymek.wspin.adapters.UserAdapter;
import szymek.wspin.hibernate.repository.CragRepository;
import szymek.wspin.hibernate.repository.RouteRepository;
import szymek.wspin.hibernate.repository.UserRepository;
import szymek.wspin.hibernate.repository.WallRepository;
import szymek.wspin.hibernate.repository.ascentRepository.AscentRepository;

@TestConfiguration
@EnableJpaRepositories(includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                CragRepository.class, AscentRepository.class, RouteRepository.class, UserRepository.class, WallRepository.class
        })
})
public class RealRepositoriesTestConfiguration {
        @MockBean
        RouteAdapter routeAdapter;

        @MockBean
        UserAdapter userAdapter;

        @MockBean
        AscentAdapter ascentAdapter;
}
