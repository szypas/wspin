package szymek.wspin.config;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/*
    https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#spring-mvc-test-server-filters
    https://engineering.pivotal.io/post/testing-spring-filters-without-pain/
    https://stackoverflow.com/questions/48748866/junit-testing-a-custom-filter-in-spring

    https://kariera.future-processing.pl/blog/exploring-spring-boot-and-spring-security-custom-token-based-authentication-of-rest-services-with-spring-security-and-pinch-of-spring-java-configuration-and-spring-integration-testing/
 */

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
class JwtFilterIT {
/*
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    JwtFilter jwtFilter;

    @MockBean
    RouteService routeService;

    @MockBean
    UserService userService;

    @MockBean
    JwtUtil jwtUtil;

    private final String GET_ASCENTS_URL = "/getAscents";
    private User USER;

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    org.springframework.security.core.userdetails.User userDetails;

    @BeforeEach
    void setup() {

        String password = "password";
        String encodedPassword = passwordEncoder.encode(password);

        USER = new User(
                "user",
                "email",
                password,
                Arrays.asList(new Role("ROLE_USER"))
        );

        userDetails = new org.springframework.security.core.userdetails.User(
                USER.getLogin(),
                encodedPassword,
                mapRolesToAuthorities(USER.getRoles()));


        when(userService.getByName(USER.getLogin())).thenReturn(USER);
        when(routeService.getUserStatuses(USER)).thenReturn(Arrays.asList());
        when(userService.loadUserByUsername(any())).thenReturn(userDetails);
    }

    @Test
    void testGetStatuses_worksWithUserAuthenticatedViaCookies() throws Exception {
        Cookie accessToken = new Cookie("access_token", "token");
        Cookie sessionId = new Cookie("JSESSIONID", "17");


        when(jwtUtil.isNotExpired("token")).thenReturn(true);
        when(jwtUtil.getUsername("token")).thenReturn(USER.getLogin());
        when(userService.loadUserByUsername(USER.getLogin())).thenReturn(userDetails);

        assertThat(this.mockMvc.perform(get(GET_ASCENTS_URL).cookie(accessToken).cookie(sessionId)).andExpect(status().isOk()));


    }

    @Test
    void testGetStatuses_worksWithUserAuthenticatedViaAuthenticationBearer() {
//        when(userService.getByName(USER.getLogin())).thenReturn(USER);
//        when(routeService.getUserStatuses(USER)).thenReturn(Arrays.asList(STATUS));
//        assertThat(this.mockMvc.perform(get(GET_ASCENTS_URL))
//                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(ROUTE_NAME)))
//                .andExpect(content().string(containsString(WALL_NAME)))
//                .andExpect(content().string(containsString(CRAG_NAME)))
//                .andExpect(content().string(containsString("6.3")))
//                .andExpect(content().string(containsString("project"))));
    }
*/
}
