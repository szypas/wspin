package szymek.wspin;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import szymek.wspin.controller.MainController;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@Import(RealRepositoriesTestConfiguration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RunAppIT {
    @LocalServerPort
    private int port;
    @Autowired
    private MainController mainController;
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void contextLoads() {
        assertThat(mainController).isNotNull();
    }

    @Test
    @WithAnonymousUser
    public void serverWorks() {
        assertThat(restTemplate.getForObject("http://localhost:" + port + "/", String.class))
                .contains("Hello");
    }
}
