package szymek.wspin.validators;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.dto.UserDto;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@Import(MockTestConfiguration.class)
@AutoConfigureMockMvc
class UserDtoValidationIT {
    private final String REGISTER_USER_URL = "/users";
    private final String JSON = "application/json;charset=UTF-8";
    @Autowired
    private MockMvc mockMvc;
    private UserDto userDto;

    @BeforeEach
    public void before() {
        userDto = new UserDto();
        userDto.setLogin("login");
        userDto.setEmail("user@email.com");
        userDto.setPassword("123");
    }

    @Test
    public void whenUserLoginIsLongerThan3AndShorterThan20_returnOKStatusCode() throws Exception {
        userDto.setLogin("correctLogin");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUserLoginIsShorterThan3_returnErrorStatusCode() throws Exception {
        userDto.setLogin("il");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenUserLoginIsLongerThan20_returnErrorStatusCode() throws Exception {
        userDto.setLogin("tooLongLogin0123456789");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenUserPasswordIsLongerThan3AndShorterThan20_returnOKStatusCode() throws Exception {
        userDto.setPassword("correctPassword");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUserPasswordIsShorterThan3_returnErrorStatusCode() throws Exception {
        userDto.setPassword("ip");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenUserPasswordIsLongerThan20_returnErrorStatusCode() throws Exception {
        userDto.setPassword("tooLongPassword0123456789");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenEmailIncorrect_returnErrorStatusCode() throws Exception {
        userDto.setEmail("incorrectemail");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenEmailIncorrect_returnOKStatusCode() throws Exception {
        userDto.setEmail("correct@email");
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUserIsNull_returnErrorStatusCode() throws Exception {
        userDto.setLogin(null);
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenEmailIsNull_returnErrorStatusCode() throws Exception {
        userDto.setEmail(null);
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenPasswordIsNull_returnErrorStatusCode() throws Exception {
        userDto.setPassword(null);
        String request = new ObjectMapper().writeValueAsString(userDto);
        mockMvc.perform(post(REGISTER_USER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }
}
