package szymek.wspin.validators;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.dto.JwtRequest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@Import(MockTestConfiguration.class)
@AutoConfigureMockMvc
class JwtRequestValidationIT {
    private final String AUTHENTICATE_REST_URL = "/authenticateRest";
    private final String AUTHENTICATE_BROWSER_URL = "/authenticateBrowser";
    private final String JSON = "application/json;charset=UTF-8";
    @Autowired
    private MockMvc mockMvc;
    private final JwtRequest jwtRequest = new JwtRequest("login", "password");

    @Test
    public void shouldReturnOk_whenRequestValid_browser() throws Exception {
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnOk_whenRequestValid_rest() throws Exception {
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_REST_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnBadRequest_whenPasswordTooLong() throws Exception {
        jwtRequest.setPassword("1234567890password1234567890");
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_REST_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequest_whenPasswordTooShort() throws Exception {
        jwtRequest.setPassword("1");
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequest_whenLoginTooLong() throws Exception {
        jwtRequest.setName("1234567890login1234567890");
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequest_whenLoginTooShort() throws Exception {
        jwtRequest.setName("1");
        String request = new ObjectMapper().writeValueAsString(jwtRequest);
        mockMvc.perform(post(AUTHENTICATE_REST_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }
}
