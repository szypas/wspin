package szymek.wspin.validators;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.util.JwtUtil;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@Import(MockTestConfiguration.class)
@AutoConfigureMockMvc
class RouteDtoValidationIT {
    private final String ROUTES_URL = "/routes";
    private final String JSON = "application/json;charset=UTF-8";
    @Autowired
    private MockMvc mockMvc;
    private JwtUtil jwtUtil;
    private RouteDto routeDto;

    @BeforeEach
    public void before() {
        routeDto = new RouteDto();
        routeDto.setRouteName("correctRouteName");
        routeDto.setCragName("correctCragName");
        routeDto.setWallName("correctWallName");
    }

    @Test
    @WithMockUser
    public void whenRouteWallCragNameCorrect_returnOKStatusCode() throws Exception {
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void whenWallNameIsShorterThan3_returnErrorStatusCode() throws Exception {
        routeDto.setWallName("in");
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenRouteNameIsShorterThan3_returnErrorStatusCode() throws Exception {
        routeDto.setRouteName("in");
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenCragNameIsShorterThan3_returnErrorStatusCode() throws Exception {
        routeDto.setCragName("in");
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenWallNameIsNull_returnErrorStatusCode() throws Exception {
        routeDto.setWallName(null);
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenRouteNameIsNull_returnErrorStatusCode() throws Exception {
        routeDto.setRouteName(null);
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenCragNameIsNull_returnErrorStatusCode() throws Exception {
        routeDto.setCragName(null);
        String request = new ObjectMapper().writeValueAsString(routeDto);
        mockMvc.perform(post(ROUTES_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }
}
