package szymek.wspin.validators;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.util.Status;

import java.sql.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class AscentValidationIT {
    private final String ASCENT_URL = "/ascents";
    private final String JSON = "application/json;charset=UTF-8";
    @Autowired
    private MockMvc mockMvc;
    private AscentDto ascentDto;

    @BeforeEach
    public void before() {
        RouteDto routeDto = new RouteDto();
        routeDto.setRouteName("correctRouteName");
        routeDto.setCragName("correctCragName");
        routeDto.setWallName("correctWallName");
        ascentDto = new AscentDto();
        ascentDto.setRouteDto(routeDto);
        ascentDto.setDifficulty("VI.3+");
        ascentDto.setStatus(Status.Project);
        ascentDto.setDate(new Date(1177));
    }

    @Test
    @WithMockUser
    public void whenStatusAndRouteFilledCorrectly_shouldReturnOk() throws Exception {
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void whenDifficultyEmpty_shouldReturnError() throws Exception {
        ascentDto.setDifficulty("");
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenDifficultyTooLong_shouldReturnError() throws Exception {
        ascentDto.setDifficulty("01234567890123");
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenStatusIsNull_shouldReturnError() throws Exception {
        ascentDto.setStatus(null);
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenDifficultyIsNull_shouldReturnError() throws Exception {
        ascentDto.setDifficulty(null);
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenRouteIsNull_shouldReturnError() throws Exception {
        ascentDto.setRouteDto(null);
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenRouteIsNotValid_shouldReturnError() throws Exception {
        ascentDto.getRouteDto().setRouteName(null);
        String request = new ObjectMapper().writeValueAsString(ascentDto);
        mockMvc.perform(post(ASCENT_URL).contentType(JSON).content(request))
                .andExpect(status().isBadRequest());
    }
}
