package szymek.wspin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.adapters.HibernateUserAdapter;
import szymek.wspin.dto.JwtRequest;
import szymek.wspin.util.JwtUtil;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class AuthControllerIT {
    private final String AUTHENTICATE_REST_URL = "/authenticateRest";
    private final String AUTHENTICATE_BROWSER_URL = "/authenticateBrowser";
    private final String USERNAME = "user765";
    private final String PASSWORD = "123erwfg";
    private final String TOKEN = "tok.123.en";
    private final String COOKIE_WITH_TOKEN = "access_token";
    private final JwtRequest jwtRequest = new JwtRequest(USERNAME, PASSWORD);
    private final ObjectMapper om = new ObjectMapper();
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private HibernateUserAdapter userService;
    @Autowired
    private JwtUtil jwtUtil;
    private String request;

    @BeforeEach
    void beforeEach() throws JsonProcessingException {
        request = om.writeValueAsString(jwtRequest);
        UserDetails userDetails = Mockito.mock(UserDetails.class);
        when(userService.loadUserByUsername(USERNAME)).thenReturn(userDetails);
        when(jwtUtil.generateToken(userDetails)).thenReturn(TOKEN);
    }

    @Test
    @WithAnonymousUser
    public void tokenIsReturnedInRestAuthentication() throws Exception {
        UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        mockMvc.perform(post(AUTHENTICATE_REST_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(TOKEN)));
        verify(authenticationManager, times(1)).authenticate(upat);
    }

    @Test
    @WithAnonymousUser
    public void tokenIsSavedToCookiesInBrowserAuthentication() throws Exception {
        UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isOk())
                .andExpect(cookie().exists(COOKIE_WITH_TOKEN))
                .andExpect(cookie().httpOnly(COOKIE_WITH_TOKEN, true))
                .andExpect(cookie().value(COOKIE_WITH_TOKEN, TOKEN));
        verify(authenticationManager, times(1)).authenticate(upat);
    }

    @Test
    @WithAnonymousUser
    public void badCredentialsExceptionThrownDuringAuthenticationForRest() throws Exception {
        UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        when(authenticationManager.authenticate(upat)).thenThrow(new BadCredentialsException("bad credentials"));
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void badCredentialsExceptionThrownDuringAuthenticationForBrowser() throws Exception {
        UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        when(authenticationManager.authenticate(upat)).thenThrow(new BadCredentialsException("bad credentials"));
        mockMvc.perform(post(AUTHENTICATE_BROWSER_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isUnauthorized());
    }
}
