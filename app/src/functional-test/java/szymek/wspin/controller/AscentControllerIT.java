package szymek.wspin.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.adapters.HibernateAscentAdapter;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.util.AscentSearchFilter;
import szymek.wspin.util.Status;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class AscentControllerIT {
    private final AscentSearchFilter FILTER = new AscentSearchFilter();
    private final Date DATE = new Date(0);
    private final String ASCENT_URL = "/ascents";
    private final String ROUTE_NAME = "Boruta";
    private final String WALL_NAME = "Diabla Skala";
    private final String CRAG_NAME = "Brzoskwinia";
    private final Status STATUS = Status.RP;
    private final String DIFFICULTY = "VI.3";
    private final String ASCENT_ID = "7";
    private final RouteDto ROUTE_DTO = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME, "0");
    private final AscentDto ASCENT_DTO = new AscentDto(ROUTE_DTO, DIFFICULTY, STATUS, DATE, ASCENT_ID);
    private final ObjectMapper om = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private HibernateAscentAdapter ascentService;
    private String ASCENT_DTO_AS_STRING;

    @BeforeEach
    private void beforeEach() throws Exception {
        ASCENT_DTO_AS_STRING = om.writeValueAsString(ASCENT_DTO);
        System.out.println(ASCENT_DTO_AS_STRING);
    }

    @Test
    @WithMockUser
    public void testGetAscents_worksWithUser() throws Exception {
        when(ascentService.getAscents(anyString(), eq(FILTER))).thenReturn(Collections.singletonList(ASCENT_DTO));
        mockMvc.perform(get(ASCENT_URL))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(ROUTE_NAME)))
                .andExpect(content().string(containsString(WALL_NAME)))
                .andExpect(content().string(containsString(CRAG_NAME)))
                .andExpect(content().string(containsString(DIFFICULTY)))
                .andExpect(content().string(containsString(STATUS.getCode())));
    }

    @Test
    @WithAnonymousUser
    public void testGetAscents_anonymousNotAuthorized() throws Exception {
        mockMvc.perform(get(ASCENT_URL))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void testAddAscent_anonymousNotAuthorized() throws Exception {
        mockMvc.perform(post(ASCENT_URL)
                        .contentType("application/json;charset=UTF-8")
                        .content(ASCENT_DTO_AS_STRING))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void testAddAscent_addAscentToNotExistingRoute_RouteAdded() throws Exception {
        mockMvc.perform(post(ASCENT_URL)
                        .contentType("application/json;charset=UTF-8")
                        .content(ASCENT_DTO_AS_STRING))
                .andExpect(status().isOk());
        verify(ascentService, times(1)).addAscent(eq(ASCENT_DTO), anyString());
    }

    @Test
    @WithMockUser
    public void testDeleteAscent() throws Exception {
        when(ascentService.getAscents(anyString(), any())).thenReturn(List.of(ASCENT_DTO));
        mockMvc.perform(delete(ASCENT_URL + "/" + ASCENT_ID))
                .andExpect(status().isNoContent());
        verify(ascentService).removeAscent(eq(ASCENT_ID), anyString());
    }
}
