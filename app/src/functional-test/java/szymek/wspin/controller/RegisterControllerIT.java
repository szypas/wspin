package szymek.wspin.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.adapters.HibernateUserAdapter;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.Role;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class RegisterControllerIT {
    private final static String REGISTER_USER_URL = "/users";
    private final static String REGISTER_ADMIN_URL = "/users-registerAdmin";
    private final static String ADMIN_NAME = "admin";
    private final static List<Role> ROLES_USER_ADMIN = Arrays.asList(new Role("ROLE_USER"), new Role("ROLE_ADMIN"));
    private final static List<String> ROLES_USER_ADMIN_AS_STRING = Arrays.asList("ROLE_USER", "ROLE_ADMIN");
    private final static UserDto USER_DTO = new UserDto("szymon", "szymon@email", "321");
    private final static ObjectMapper om = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private HibernateUserAdapter userServiceMock;
    private String request;

    @BeforeEach
    void beforeEach() throws Exception {
        request = om.writeValueAsString(USER_DTO);
    }

    @Test
    @WithAnonymousUser
    public void userAlreadyRegistered() throws Exception {
        given(userServiceMock.getByName(USER_DTO.getLogin())).willReturn(USER_DTO);
        mockMvc.perform(post(REGISTER_USER_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isUnprocessableEntity());
        verify(userServiceMock, times(0)).save(USER_DTO);
    }

    @Test
    @WithAnonymousUser
    public void anonymousCanRegisterUser() throws Exception {
        String expectedUser = om.writeValueAsString(USER_DTO);
        when(userServiceMock.save(USER_DTO)).thenReturn(USER_DTO);
        mockMvc.perform(post(REGISTER_USER_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isOk()).andExpect(content().string(expectedUser));
    }

    @Test
    @WithMockUser(username = ADMIN_NAME, roles = {"USER", "ADMIN"})
    public void adminAlreadyRegistered() throws Exception {
        given(userServiceMock.getByName(USER_DTO.getLogin())).willReturn(USER_DTO);
        mockMvc.perform(post(REGISTER_ADMIN_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isUnprocessableEntity());
        verify(userServiceMock, times(0)).save(any());
        verify(userServiceMock, times(0)).save(any(), any());
    }

    @Test
    @WithMockUser(username = ADMIN_NAME, roles = {"USER", "ADMIN"})
    public void adminCanRegisterAdmin() throws Exception {
        String expectedUser = om.writeValueAsString(USER_DTO);
        when(userServiceMock.save(USER_DTO, ROLES_USER_ADMIN_AS_STRING)).thenReturn(USER_DTO);
        mockMvc.perform(post(REGISTER_ADMIN_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isOk()).andExpect(content().string(expectedUser));
    }

    @Test
    @WithAnonymousUser
    public void anonymousCannotRegisterAdmin() throws Exception {
        mockMvc.perform(post(REGISTER_ADMIN_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isUnauthorized());
        verify(userServiceMock, times(0)).save(any());
        verify(userServiceMock, times(0)).save(any(), any());
    }

    @Test
    @WithMockUser
    public void userCanRegisterAdmin() throws Exception {
        String expectedUser = om.writeValueAsString(USER_DTO);
        when(userServiceMock.save(USER_DTO, ROLES_USER_ADMIN_AS_STRING)).thenReturn(USER_DTO);
        mockMvc.perform(post(REGISTER_ADMIN_URL).contentType("application/json;charset=UTF-8").content(request))
                .andExpect(status().isOk()).andExpect(content().string(expectedUser));
    }
}
