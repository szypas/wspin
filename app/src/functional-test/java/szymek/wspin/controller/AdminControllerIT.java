package szymek.wspin.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.adapters.HibernateUserAdapter;
import szymek.wspin.dto.UserDto;
import szymek.wspin.hibernate.model.User;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class AdminControllerIT {
    private final String GET_ALL_USERS_URL = "/users";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private HibernateUserAdapter userServiceMock;

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void getUsersWorksWithAdminAccount() throws Exception {
        User user = new User("user", "user@email.pl", "321");
        UserDto userDto = new UserDto("user", "user@email.pl", "321");
        given(userServiceMock.getAllUsers()).willReturn(Collections.singletonList(userDto));
        mockMvc.perform(get(GET_ALL_USERS_URL))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("user")))
                .andExpect(content().string(containsString("user@email.pl")));
    }

    @Test
    @WithAnonymousUser
    public void getUsers_anonymousNotAuthenticated() throws Exception {
        mockMvc.perform(get(GET_ALL_USERS_URL))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void getUsers_userNotAuthorized() throws Exception {
        mockMvc.perform(get(GET_ALL_USERS_URL))
                .andExpect(status().isForbidden());
    }
}
