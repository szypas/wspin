package szymek.wspin.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import szymek.wspin.MockTestConfiguration;
import szymek.wspin.adapters.HibernateRouteAdapter;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.hibernate.model.Crag;
import szymek.wspin.hibernate.model.Route;
import szymek.wspin.hibernate.model.Wall;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
@Import(MockTestConfiguration.class)
class RouteControllerIT {
    private final String ROUTES_URL = "/routes";
    private final String ROUTE_NAME = "Boruta";
    private final String WALL_NAME = "Diabla Skala";
    private final String CRAG_NAME = "Brzoskwinia";
    private final Route ROUTE = new Route(ROUTE_NAME, new Wall(WALL_NAME, new Crag(CRAG_NAME)));
    private final RouteDto ROUTE_DTO = new RouteDto(ROUTE_NAME, WALL_NAME, CRAG_NAME, "0");
    private final ObjectMapper om = new ObjectMapper();
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private HibernateRouteAdapter routeService;
    private String ROUTE_DTO_REQUEST;

    @BeforeEach
    private void beforeEach() throws Exception {
        ROUTE_DTO_REQUEST = om.writeValueAsString(ROUTE_DTO);
    }

    @Test
    @WithAnonymousUser
    public void testGetRoutes() throws Exception {
        List<RouteDto> routes = new ArrayList<>(Collections.singletonList(ROUTE_DTO));
        when(routeService.getAllRoutes()).thenReturn(routes);
        mockMvc.perform(get(ROUTES_URL))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(ROUTE_NAME)));
        verify(routeService, times(1)).getAllRoutes();
    }

    @Test
    @WithMockUser
    public void testAddRoutes_routeAlreadyExists() throws Exception {
        when(routeService.getRoute(ROUTE_DTO)).thenReturn(ROUTE_DTO);
        mockMvc.perform(post(ROUTES_URL).contentType("application/json;charset=UTF-8").content(ROUTE_DTO_REQUEST))
                .andExpect(status().isUnprocessableEntity());
        verify(routeService, times(0)).addRoute(any());
    }

    @Test
    @WithMockUser
    public void testAddRoutes_routeDoesntExists() throws Exception {
        when(routeService.getRoute(any())).thenReturn(null);
        when(routeService.addRoute(ROUTE_DTO)).thenReturn(ROUTE_DTO);
        mockMvc.perform(post(ROUTES_URL).contentType("application/json;charset=UTF-8").content(ROUTE_DTO_REQUEST))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(ROUTE.getName())))
                .andExpect(content().string(containsString(ROUTE.getWall().getName())))
                .andExpect(content().string(containsString(ROUTE.getWall().getCrag().getName())));
    }

    @Test
    @WithMockUser
    public void testDeleteRoute() throws Exception {
        String id = "7";
        mockMvc.perform(delete(ROUTES_URL + "/" + id))
                .andExpect(status().isNoContent());
        verify(routeService, times(1)).deleteRoute(id);
    }

    @Test
    @WithMockUser
    public void testDeleteRoute_whenDataIntegrityViolationThrown() throws Exception {
        String id = "7";
        doThrow(DataIntegrityViolationException.class).when(routeService).deleteRoute(id);
        mockMvc.perform(delete(ROUTES_URL + "/" + id))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Route cannot be deleted")));
    }
}
