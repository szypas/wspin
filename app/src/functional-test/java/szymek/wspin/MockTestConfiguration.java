package szymek.wspin;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import szymek.wspin.adapters.HibernateAscentAdapter;
import szymek.wspin.adapters.HibernateRouteAdapter;
import szymek.wspin.adapters.HibernateUserAdapter;
import szymek.wspin.util.JwtUtil;

@TestConfiguration
public class MockTestConfiguration {
    @MockBean
    HibernateUserAdapter userService;
    @MockBean
    HibernateRouteAdapter routeService;
    @MockBean
    HibernateAscentAdapter ascentService;
    @MockBean
    JwtUtil jwtUtil;
    @MockBean
    AuthenticationManager authenticationManager;
}
