package szymek.wspin.util;

public enum Status {
    Project("Project"),
    Todo("Todo"),
    Todo_Tried("Todo_Tried"),
    Tried("Tried"),
    RP("RP"),
    FL("FL"),
    OS("OS");
    private final String code;

    Status(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
