package szymek.wspin.util.difficulty;

import lombok.extern.slf4j.Slf4j;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class DifficultyUtil {
    public final static List<String> DIFFICULTIES = new ArrayList<>(
            Arrays.asList(
                    "I", "II", "III", "IV", "IV+", "V-", "V", "V+", "VI-", "VI", "VI+",
                    "VI.1", "VI.1+", "VI.2", "VI.2+", "VI.3", "VI.3+", "VI.4", "VI.4+", "VI.5", "VI.5+",
                    "VI.6", "VI.6+", "VI.7", "VI.7+", "VI.8", "VI.8+", "VI.9", "VI.9+"));

    public static List<String> generateListOfDifficulties(String from, String to) {
        int indexFrom = DIFFICULTIES.indexOf(from);
        int indexTo = DIFFICULTIES.indexOf(to);
        if (indexFrom < 0 || indexTo < 0) throw new InvalidParameterException(
                "one or both parameters out of range. to: " + to + "/" + indexTo + "; from: " + from + "/" + indexFrom + ".");
        if (indexFrom > indexTo) throw new InvalidParameterException("'from' should be smaller than 'to'");
        List<String> difficulties = new ArrayList<>();
        for (int i = indexFrom; i <= indexTo; i++) {
            difficulties.add(DIFFICULTIES.get(i));
        }
        return difficulties;
    }
}
