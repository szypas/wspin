package szymek.wspin.util.difficulty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DifficultyValidator implements ConstraintValidator<DifficultyConstraint, String> {
    @Override
    public void initialize(DifficultyConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) return false;
        return DifficultyUtil.DIFFICULTIES.contains(s);
    }
}
