package szymek.wspin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RouteDto {
    @NotNull
    @Length(min = 3)
    private String routeName;

    @NotNull
    @Length(min = 3)
    private String wallName;

    @NotNull
    @Length(min = 3)
    private String cragName;
    private String id;

    public RouteDto(String routeName, String wallName, String cragName) {
        this.routeName = routeName;
        this.wallName = wallName;
        this.cragName = cragName;
    }

    public void setId(String id) {
        this.id = id;
    }
}
