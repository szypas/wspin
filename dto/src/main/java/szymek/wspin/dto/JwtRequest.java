package szymek.wspin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtRequest {

    @NotNull
    @Length(min=3, max=20)
    private String name;

    @NotNull
    @Length(min=3, max=20)
    private String password;
}