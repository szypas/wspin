package szymek.wspin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    @NotNull
    @Length(min=3, max=20)
    private String login;

    @NotNull
    @NotEmpty
    @Email
    private String email;

    @NotNull
    @Length(min=3, max=20)
    private String password;
    private Collection<String> roles;

    public UserDto(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }
}
