package szymek.wspin.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import szymek.wspin.util.Status;
import szymek.wspin.util.difficulty.DifficultyConstraint;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AscentDto {
    @Valid
    @NotNull
    private RouteDto routeDto;

    @DifficultyConstraint
    private String difficulty;

    @NotNull
    private Status status;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;

    private String id;

    public AscentDto(RouteDto routeDto, String difficulty, Status status, Date date) {
        this.routeDto = routeDto;
        this.difficulty = difficulty;
        this.status = status;
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }
}
