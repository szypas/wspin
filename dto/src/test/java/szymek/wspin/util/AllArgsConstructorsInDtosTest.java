package szymek.wspin.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import szymek.wspin.dto.AscentDto;
import szymek.wspin.dto.JwtRequest;
import szymek.wspin.dto.RouteDto;
import szymek.wspin.dto.UserDto;

import java.sql.Date;

class AllArgsConstructorsInDtosTest {
    private final RouteDto routeDto = new RouteDto("routeName", "wallName", "cragName");
    private final AscentDto ascentDto = new AscentDto(routeDto, "difficulty", Status.Todo_Tried, new Date(0));
    private final UserDto userDto = new UserDto("login", "email", "password");
    private final JwtRequest jwtRequest = new JwtRequest("login", "password");

    @Test
    void testRouteMembers_shouldBeInCorrectOrder() {
        Assertions.assertEquals("routeName", routeDto.getRouteName());
        Assertions.assertEquals("wallName", routeDto.getWallName());
        Assertions.assertEquals("cragName", routeDto.getCragName());
    }

    @Test
    void testAscentMembers_shouldBeInCorrectOrder() {
        Assertions.assertEquals(routeDto, ascentDto.getRouteDto());
        Assertions.assertEquals("difficulty", ascentDto.getDifficulty());
        Assertions.assertEquals(Status.Todo_Tried, ascentDto.getStatus());
    }

    @Test
    void testUserMembers_shouldBeInCorrectOrder() {
        Assertions.assertEquals("login", userDto.getLogin());
        Assertions.assertEquals("email", userDto.getEmail());
        Assertions.assertEquals("password", userDto.getPassword());
    }

    @Test
    void testJwtRequestMembers_shouldBeInCorrectOrder() {
        Assertions.assertEquals("login", jwtRequest.getName());
        Assertions.assertEquals("password", jwtRequest.getPassword());
    }
}
