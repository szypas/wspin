package szymek.wspin.util;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import szymek.wspin.util.difficulty.DifficultyValidator;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DifficultyConstraintTest {
    private final DifficultyValidator constraint = new DifficultyValidator();
    private final ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);

    @Test
    void testNullDifficulty() {
        assertFalse(constraint.isValid(null, context));
    }

    @Test
    void testEmptyDifficulty() {
        assertFalse(constraint.isValid("", context));
    }

    @Test
    void testRandomDifficulties() {
        assertTrue(constraint.isValid("IV", context));
        assertTrue(constraint.isValid("V+", context));
        assertTrue(constraint.isValid("VI-", context));
        assertTrue(constraint.isValid("VI", context));
        assertTrue(constraint.isValid("VI.1", context));
        assertTrue(constraint.isValid("VI.1+", context));
        assertTrue(constraint.isValid("VI.3", context));
    }

    @Test
    void testIncorrectDifficulties() {
        assertFalse(constraint.isValid("VI.10", context));
        assertFalse(constraint.isValid("VI.", context));
        assertFalse(constraint.isValid("VI.1-", context));
        assertFalse(constraint.isValid("V.1-", context));
        assertFalse(constraint.isValid("IV.1", context));
        assertFalse(constraint.isValid("IV-", context));
        assertFalse(constraint.isValid("III+", context));
        assertFalse(constraint.isValid("VI.2/+", context));
        assertFalse(constraint.isValid("VI.2+/3", context));
    }
}
