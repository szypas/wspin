package szymek.wspin.util;

import org.junit.jupiter.api.Test;
import szymek.wspin.util.difficulty.DifficultyUtil;

import java.security.InvalidParameterException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DifficultyUtilTest {
    @Test
    void testValidFromTo() {
        List<String> list = DifficultyUtil.generateListOfDifficulties("III", "VI.3+");
        assertEquals(15, list.size());
        assertSame("VI.3+", list.get(list.size() - 1));
        assertSame("III", list.get(0));
    }

    @Test
    void testValidFromTo2() {
        List<String> list = DifficultyUtil.generateListOfDifficulties("I", "VI.9+");
        assertEquals(29, list.size());
        assertSame("VI.9+", list.get(list.size() - 1));
        assertSame("I", list.get(0));
    }

    @Test
    void testValidFromTo3() {
        List<String> list = DifficultyUtil.generateListOfDifficulties("VI.1", "VI.4");
        assertEquals(7, list.size());
        assertSame("VI.4", list.get(list.size() - 1));
        assertSame("VI.1", list.get(0));
    }

    @Test
    void testInvalidFromTo_FromOutOfRange() {
        assertThrows(InvalidParameterException.class, () -> DifficultyUtil.generateListOfDifficulties("VI.12", "VI.1"));
    }

    @Test
    void testInvalidFromTo_ToOutOfRange() {
        assertThrows(InvalidParameterException.class, () -> DifficultyUtil.generateListOfDifficulties("VI.1", "VI.11"));
    }

    @Test
    void testInvalidFromTo_BothOutOfRange() {
        assertThrows(InvalidParameterException.class, () -> DifficultyUtil.generateListOfDifficulties("VI.11", "VI.51"));
    }

    @Test
    void testInvalidFromTo_parametersOrderIncorrect() {
        assertThrows(InvalidParameterException.class, () -> DifficultyUtil.generateListOfDifficulties("VI.5", "VI.4"));
    }
}
